import React, { memo, useEffect, useRef, useState } from 'react';
import { Button, FlatListQuestion, Spinner } from '@/components/HOC';
import { useTranslation } from 'react-i18next';
import { DynamicHeader } from '@/components/Header';
import { navigate } from '@/utils/navigation';
import { useIsFocused, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { approveQuestion, getListQuestion, getRandomQuestion } from '@/services';
import { Alert, Image, Text, View } from 'react-native';
import { setPackages } from '@/redux/PackagesSlice';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { width_screen } from '@/utils';
import icons from '@/assets/icons';
import { SelectModal } from '@/components/Modal';

const AllQuestion = memo(() => {
    const isFocused = useIsFocused();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [isLoadingMore, setLoadingMore] = useState(false);
    const [questions, setQuestions] = useState([]);
    const [count, setCount] = useState(0);
    const [list, setList] = useState([]);

    const [data, setData] = useState({
        pageIndex: 1,
        pageSize: 20,
        type: 1
    })

    const onAddManyQuestion = () => {
        let data: any = [...list];
        let newArr = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].isChoosed) {
                newArr.push(data[i].id)
            }
        }
        let dataAdmin = {
            questions: newArr,
            status: 0
        }
        approveQuestion(dataAdmin, token, (res: any) => {
            if (res.code == "00") {
                Alert.alert('Thông báo', 'Đã duyệt thành công các câu hỏi!')
                navigate('MainStack')
            }
        })
    }

    useEffect(() => {
        if (isFocused) {
            getListQuestion(data, token, (res: any) => {
                setLoadingMore(false);
                if (res.code == "00") {
                    if (res.data && res.data.items && res.data.items.length > 0){
                        setQuestions((preArr: any) => preArr.concat(res?.data?.items))
                    }
                    setCount(res?.data?.meta?.count)
                }
            })
        }
    }, [isFocused])

    const getAPI = () => {
        let preData = data;
        if (preData.pageIndex < count / 20 && count != 0) {
            console.log('load more ...')
            let newData = {
                ...preData,
                pageIndex: preData.pageIndex + 1
            }
            setData(newData)
            setLoadingMore(true);
            getListQuestion(newData, token, (res: any) => {
                setLoadingMore(false);
                console.log('get list next ques', res)
                if (res.code == "00" && res.data && res.data.items && res.data.items.length > 0) {
                    setQuestions((preArr: any) => preArr.concat(res?.data?.items))
                }
            })
        }
    }

    return (
        <>
            <DynamicHeader title={'Duyệt câu hỏi'} back />
            <View style={{ flex: 1 }}>
                {questions && questions.length > 0 ?
                    <FlatListQuestion
                        data={questions}
                        onChooseMulti={(items: any) => setList(items)}
                        onLoadMore={getAPI}
                        isLoadingMore={isLoadingMore}
                        setData={setQuestions}
                    />
                    : <Text style={{ marginTop: 100, alignSelf: 'center' }}>Chưa có câu hỏi nào</Text>
                }
            </View>
            <View style={{ backgroundColor: 'white', alignItems: 'center', paddingVertical: 10 }}>
                <Button width={width_screen * 0.5} onPress={() => onAddManyQuestion()}>Duyệt</Button>
            </View>
        </>
    );
});

export default AllQuestion;