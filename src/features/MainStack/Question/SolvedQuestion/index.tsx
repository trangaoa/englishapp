import React, { memo, useEffect, useState } from 'react';
import { EmptyState, FlatListQuestion, Spinner } from '@/components/HOC';
import { useTranslation } from 'react-i18next';
import { DynamicHeader } from '@/components/Header';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { getListQuestion } from '@/services';

const SolvedQuestion = memo(() => {
    const { t } = useTranslation();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [questions, setQuestions] = useState([]);
    const [isLoading, setLoading] = useState(true);

    let data = {
        pageIndex: 1,
        pageSize: 40,
        type: 3
    }

    useEffect(() => {
        getListQuestion(data, token, (res: any) => {
            setLoading(false)
            if (res.code == "00" && res.data) {
                setQuestions(res.data?.items ? res.data?.items : [])
            }
        })
    }, [])

    return (
        <>
            <DynamicHeader title={'Câu hỏi đã làm'} back />
            {isLoading ?
                <Spinner /> :
                (questions && questions.length > 0 ?
                    <FlatListQuestion
                        data={questions}
                    />
                    : <EmptyState content={'Bạn chưa làm câu hỏi nào cả'} />
                )
            }
        </>
    );
});

export default SolvedQuestion;