import icons from '@/assets/icons';
import { DynamicHeader } from '@/components/Header';
import { Button } from '@/components/HOC';
import { font, font_size } from '@/configs/fonts';
import {  width_screen } from '@/utils';
import { navigate } from '@/utils/navigation';
import React, { memo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, TouchableOpacity, Image, TextInput, ScrollView, Alert } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

const CreateQuestion = memo(() => {
    const { t } = useTranslation();
    const [value, setValue] = useState('');
    const [listAnswers, setList] = useState([{
        id: 0,
        content: '',
        explain: '',
        isCorrect: false
    }]);

    const RenderHeader = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => onNext()}>
                <Text style={{ color: 'white', paddingVertical: 5, paddingHorizontal: 10, backgroundColor: '#314C1C', borderRadius: 30, marginRight: 15 }}>Tiếp</Text>
            </TouchableOpacity>
        )
    }

    const onNext = () => {
        let answerList = [...listAnswers];
        let length = answerList.length;
        if (value == '') {
            Alert.alert('Thông báo', `Vui lòng điền nội dung câu hỏi`)
        } else {
            if (length > 1 && length < 5) {
                let lastItem = answerList[length - 1];
                if (lastItem.content == '') {
                    Alert.alert('Thông báo', `Vui lòng điền đáp án thứ ${length}`)
                } else {
                    navigate('AddQuestion', { data: { title: value, items: answerList } })
                }
            } else {
                Alert.alert('Thông báo', 'Mỗi câu hỏi có từ 2-4 đáp án')
            }
        }
    }

    const onAddAnswer = () => {
        let newData = [...listAnswers];
        let length = newData.length;
        if (length > 0 && length < 5) {
            let lastItem = newData[length - 1]
            if (lastItem.content) {
                newData.push({
                    id: lastItem.id + 1,
                    content: '',
                    explain: '',
                    isCorrect: false
                })
                setList(newData);
            } else {
                Alert.alert('Thông báo', 'Vui lòng điền nội dung đáp án trước')
            }
        } else {
            if (length == 0){
                newData.push({
                    id: 1,
                    content: '',
                    explain: '',
                    isCorrect: false
                })
                setList(newData);
            } else Alert.alert('Thông báo', 'Mỗi câu hỏi có từ 2-4 đáp án')
        }
    }

    const onUpdateAnswer = (item: any, type: any, data: any) => {
        let newData = [...listAnswers];
        let length = newData.length;
        for (let i = 0; i < length; i++) {
            if (newData[i].id == item.id) {
                newData[i] = {
                    ...item,
                    [type]: data
                }
            }
        }
        setList(newData)
    }

    const onDeleteAnswer = (item: any) => {
        setList((preData) => {
            let newData = [...preData];
            for (let i = 0; i < newData.length; i++) {
                if (newData[i].id == item.id) {
                    newData.splice(i, 1)
                }
            }
            return newData;
        });
    }

    return (
        <>
            <DynamicHeader
                title={'Tạo câu hỏi'}
                back
                Header={<RenderHeader />}
            />
            <ScrollView style={{ flex: 1, backgroundColor: 'white' }} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <Text style={styles.txtHeader}>Câu hỏi:</Text>
                    <TextInput
                        style={styles.txtInput}
                        value={value}
                        onChangeText={text => setValue(text)}
                        multiline={true}
                        underlineColorAndroid='transparent'
                        placeholder={'Vui lòng nhập câu hỏi'}
                    />
                    <Text style={styles.txtHeader}>Đáp án: <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }}>(Mỗi câu hỏi có từ 2-4 đáp án)</Text></Text>
                    {
                        listAnswers.map((item: any, index: any) => {
                            return (
                                <View style={styles.viewAnswer} key={index}>
                                    <View style={[styles.viewRow, { justifyContent: 'space-between' }]}>
                                        <Text style={styles.txtNumber}>{index == 0 ? 'A' : (index == 1 ? 'B' : (index == 2 ? 'C' : 'D'))}.</Text>
                                        <TouchableOpacity style={styles.viewRow} activeOpacity={0.7} onPress={() => onDeleteAnswer(item)}>
                                            <Image source={icons.other.close} style={styles.icoClose} />
                                            <Text style={styles.txtDelete}>Xóa đáp án</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <TextInput
                                        placeholder={`Vui lòng nhập đáp án ${index + 1} của câu hỏi`}
                                        value={item.content ? item.content.toString() : ''}
                                        onChangeText={(text: any) => onUpdateAnswer(item, 'content', text)}
                                        style={styles.txtAnswer}
                                    />
                                    <Text style={styles.txtDesc}>Giải thích</Text>
                                    <TextInput
                                        placeholder={`Giải thích đáp án (Bạn có thể bỏ qua phần này)`}
                                        value={item.explain ? item.explain.toString() : ''}
                                        onChangeText={(text: any) => onUpdateAnswer(item, 'explain', text)}
                                        style={styles.txtExplain}
                                        multiline={true}
                                    />
                                </View>
                            )
                        })
                    }
                </View>
                <View style={{height: 40}}/>
            </ScrollView>
            <View style={{ alignItems: 'center', backgroundColor: 'white', paddingVertical: 10 }}>
                <Button width={width_screen * 0.4} onPress={() => onAddAnswer()}>Thêm đáp án</Button>
            </View>
        </>
    );
});

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: '15@ms'
    },
    txtHeader: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        marginTop: '15@ms',
        marginBottom: '5@ms'
    },
    txtInput: {
        borderColor: '#314C1C',
        borderWidth: 0.5,
        borderRadius: 10,
        height: '120@ms',
        textAlignVertical: 'top',
        padding: '10@ms'
    },
    viewAnswer: {

    },
    txtNumber: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_SMALL * 2,
        color: '#314C1C',
        marginTop: '10@ms',
        lineHeight: '32@ms'
    },
    txtAnswer: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        borderBottomWidth: 0.75,
        padding: 0,
        borderBottomColor: '#3F6766'
    },
    txtExplain: {
        borderWidth: 0.5,
        borderColor: '#314C1C',
        height: 80,
        borderRadius: '8@ms',
        textAlignVertical: 'top',
        padding: '10@ms'
    },
    txtDesc: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        marginVertical: '10@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icoClose: {
        width: '10@ms',
        height: '10@ms',
        marginHorizontal: 5
    },
    txtDelete: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_SMALL,
        color: '#3F6766'
    }
});

export default CreateQuestion;