import React, { memo,  useState } from 'react';
import { Image,Text, TouchableOpacity, View } from 'react-native';
import { FlatListQuestion } from '@/components/HOC';
import { useTranslation } from 'react-i18next';
import { DynamicHeader } from '@/components/Header';
import { useRoute } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size } from '@/configs/fonts';

const QuestionDetail = memo(() => {
    const { t } = useTranslation();
    const route = useRoute<any>();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [products, setProducts] = useState([1]);

    return (
        <>
            <DynamicHeader title={'Chi tiết câu hỏi'} back/>
            <View style={styles.viewAccount}>
                <Image
                source={{uri: 'https://kenh14cdn.com/2020/8/10/photo-1-1597051380162862366200.jpg'}}
                style={styles.imgAvatar}
                />
                <View style={{justifyContent: 'flex-start'}}>
                    <Text style={styles.txtUsername}>Yoonabar</Text>
                    <TouchableOpacity>
                        <Text style={styles.txtFollow}>Follow</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <FlatListQuestion
                data={products}
            />
        </>
    );
});

export default QuestionDetail;

const styles = ScaledSheet.create({
    container: {
        // marginBottom: '20@ms'
    },
    viewAccount: {
        backgroundColor: '#314C1C',
        borderBottomRightRadius: '40@ms',
        borderBottomLeftRadius: '40@ms',
        paddingHorizontal: '40@ms',
        paddingVertical: '15@ms',
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgAvatar: {
        width: '60@ms',
        height: '60@ms',
        borderRadius: '30@ms',
        borderWidth: 1,
        borderColor: 'white',
        marginRight: '15@ms'
    },
    txtUsername: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_LARGE + 4,
        color: 'white'
    },
    txtFollow: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        marginTop: '5@ms',
        paddingVertical: '3@ms',
        paddingHorizontal: '15@ms',
        backgroundColor: 'white',
        color: '#314C1C',
        borderRadius: '20@ms',
        textAlign: 'center',
        alignSelf: 'flex-start'
    }
});