import React, { memo, useEffect, useRef, useState } from 'react';
import { Button, FlatListQuestion, Spinner } from '@/components/HOC';
import { useTranslation } from 'react-i18next';
import { DynamicHeader, SearchHeader } from '@/components/Header';
import { navigate } from '@/utils/navigation';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { getListQuestion } from '@/services';
import { Image, Text, View } from 'react-native';
import { setPackages } from '@/redux/PackagesSlice';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { width_screen } from '@/utils';
import icons from '@/assets/icons';
import { SelectModal } from '@/components/Modal';

const SearchQuestion = memo(() => {
    const { t } = useTranslation();
    const filterRef: any = useRef();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [isLoading, setLoading] = useState(false);
    const [isLoadingMore, setLoadingMore] = useState(false);
    const [questions, setQuestions] = useState([]);
    const [count, setCount] = useState(0);
    const [index, setIndex] = useState(1);
    const [keyword, setKeyword] = useState('')

    let listFilter = [
        { id: 0, name: 'Tất cả câu hỏi' },
        { id: 1, name: 'Mức độ dễ' },
        { id: 2, name: 'Mức độ trung bình' },
        { id: 3, name: 'Mức độ khó' },
        { id: 4, name: 'Câu hỏi của tôi' },
        { id: 5, name: 'Câu hỏi đã làm' },
        { id: 6, name: 'Câu hỏi chưa làm' }
    ]
    const [data, setData]: any = useState({
        keyword: '',
        pageIndex: 1,
        pageSize: 20,
        type: 0
    })

    const onSelectFilter = (item: any) => {
        if (item.id == 0) {
            setData({
                search: keyword,
                pageIndex: 1,
                pageSize: 20,
                type: 0
            })
        } else {
            if (item.id == 1 || item.id == 2 || item.id == 3) {
                setData({
                    search: keyword,
                    pageIndex: 1,
                    pageSize: 20,
                    level: item.id,
                    type: 0
                })
            } else {
                setData({
                    search: keyword,
                    pageIndex: 1,
                    pageSize: 20,
                    type: item.id - 2
                })
            }
        }
    }

    useEffect(() => {
        getAPI()
    }, [data])

    const onChangeKeyword = (text: any) => {
        setKeyword(text)
        setData({
            ...data,
            pageIndex: 1,
            search: text
        })
    }

    const getAPI = () => {
        setLoading(true)
        getListQuestion(data, token, (res: any) => {
            setLoading(false);
            console.log('get list ques', res)
            if (res.code == "00" && res.data && res.data.items && res.data.items.length > 0) {
                let arr = res?.data?.items;
                for (let i = 0; i < arr.length; i++) {
                    arr[i] = {
                        ...arr[i],
                        isChoosed: false
                    }
                }
                setQuestions(arr)
                setCount(res.data.meta.count)
            } else {
                setQuestions([])
            }
        })
    }

    const onLoadMore = () => {
        if (index < count / 20) {
            setIndex(index + 1)
            let newData = {
                ...data,
                pageIndex: index + 1
            }
            setLoadingMore(true);
            getListQuestion(newData, token, (res: any) => {
                setLoadingMore(false);
                console.log('get list next ques', res)
                if (res.code == "00" && res.data && res.data.items && res.data.items.length > 0) {
                    setQuestions((preArr: any) => preArr.concat(res?.data?.items))
                }
            })
        }
    }

    return (
        <>
            <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
                <SearchHeader keyword={keyword} back setKeyword={onChangeKeyword} />
                <TouchableOpacity activeOpacity={0.7} onPress={() => filterRef.current.open()}>
                    <Image source={icons.header.filter} style={{ width: 24, height: 24, marginRight: 15 }} />
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
                {isLoading ?
                    <Spinner /> :
                    (questions && questions.length > 0 ?
                        <FlatListQuestion
                            data={questions}
                            onLoadMore={onLoadMore}
                            isLoadingMore={isLoadingMore}
                            setData={setQuestions}
                        />
                        : <Text style={{ marginTop: 100, alignSelf: 'center' }}>Chưa có câu hỏi nào</Text>
                    )
                }
            </View>
            <SelectModal
                modalRef={filterRef}
                data={{
                    title: 'Bộ lọc câu hỏi',
                    options: listFilter
                }}
                onPress={(item: any) => onSelectFilter(item)}
            />
        </>
    );
});

export default SearchQuestion;