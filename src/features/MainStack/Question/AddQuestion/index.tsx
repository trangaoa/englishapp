import { DynamicHeader } from '@/components/Header';
import { Button } from '@/components/HOC';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import React, { memo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, TouchableOpacity, Image, Alert } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { useSelector } from 'react-redux';
import { RadioButton, Switch } from 'react-native-paper';
import { useRoute } from '@react-navigation/core';
import { SelectModal } from '@/components/Modal';
import icons from '@/assets/icons';
import { createQuestion } from '@/services';
import { navigate } from '@/utils/navigation';

const AddQuestion = memo(() => {
    const { t } = useTranslation();
    const route: any = useRoute();
    const levelRef: any = useRef(null);
    const data = route?.params?.data;
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [checked, setChecked] = useState(0);
    const [isHidden, setHidden] = useState(false);
    const [isShuffle, setShuffle] = useState(false);

    let listLevel = [
        {
            id: 1,
            name: 'Dễ'
        },
        {
            id: 2,
            name: 'Trung bình'
        },
        {
            id: 3,
            name: 'Khó'
        }
    ];
    const [level, setLevel] = useState<any>(listLevel[0]);

    const RenderHeader = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => onCreate()}>
                <Text style={{ color: 'white', paddingVertical: 5, paddingHorizontal: 10, backgroundColor: '#314C1C', borderRadius: 30, marginRight: 15 }}>Tạo</Text>
            </TouchableOpacity>
        )
    }

    const onCreate = () => {
        let answers = [...data.items];
        for (let i = 0; i < answers.length; i++) {
            console.log('i', checked)
            answers[i].isCorrect = i == checked
        }
        let dataQuestion = {
            title: data.title,
            level: level.id,
            isHidden: isHidden,
            answers: answers
        }
        createQuestion(dataQuestion, token, (res: any) => {
            console.log("create ques", res)
            if (res.code == "00"){
                Alert.alert('Thông báo', 'Bạn đã tạo thành công câu hỏi. Hệ thống sẽ duyệt câu hỏi của bạn sớm');
                navigate('MainStack')
            } else {
                Alert.alert('Thông báo', 'Lỗi tạo câu hỏi');
            }
        })
    }

    return (
        <>
            <DynamicHeader
                title={'Tạo câu hỏi'}
                back
                Header={<RenderHeader />}
            />
            <View style={styles.container}>
                <Text style={styles.txtHeader}>Câu hỏi:</Text>
                <Text style={styles.txtQuestion}>{data.title}</Text>
                <Text style={styles.txtHeader}>Đáp án đúng:</Text>
                <View style={{ marginLeft: 25 }}>
                    {data.items.map((item: any, index: any) => {
                        return (
                            <View style={styles.viewRow} key={index}>
                                <RadioButton
                                    value={index}
                                    status={checked === index ? 'checked' : 'unchecked'}
                                    onPress={() => setChecked(index)}
                                    color={'#314C1C'}
                                />
                                <Text style={styles.txtAnswer}>{index == 0 ? 'A' : (index == 1 ? 'B' : (index == 2 ? 'C' : 'D'))}. {item.content}</Text>
                            </View>
                        )
                    })
                    }
                </View>
                <TouchableOpacity style={styles.viewSpace} activeOpacity={0.7} onPress={() => levelRef.current.open()}>
                    <Text style={styles.txtHeader1}>Độ khó câu hỏi</Text>
                    <View style={styles.viewRow}>
                        <Text style={styles.txtHeader1}>{level.name}</Text>
                        <Image
                            source={icons.other.arrowDown}
                            style={styles.icoDown}
                        />
                    </View>
                </TouchableOpacity>
                {/* <Text style={styles.txtHeader}>Chế độ hiển thị</Text> */}
                <View style={styles.viewSpace}>
                    <Text style={styles.txtHeader1}>Hiển thị tên tác giả</Text>
                    <Switch value={!isHidden} onValueChange={setHidden} color={'#314C1C'} children={undefined} onChange={undefined} />
                </View>
                <View style={styles.viewSpace}>
                    <Text style={styles.txtHeader1}>Sắp xếp đáp án ngẫu nhiên</Text>
                    <Switch value={isShuffle} onValueChange={setShuffle} color={'#314C1C'} children={undefined} onChange={undefined} />
                </View>
            </View>
            <SelectModal
                modalRef={levelRef}
                data={{
                    title: 'Mức độ khó',
                    options: listLevel
                }}
                onPress={(item: any) => setLevel(item)}
            />
        </>
    );
});

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    txtHeader: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        paddingHorizontal: '15@ms',
        marginTop: '15@ms'
    },
    txtHeader1: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        paddingHorizontal: '15@ms'
    },
    txtQuestion: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        borderBottomColor: '#314C1C',
        borderBottomWidth: 1,
        paddingHorizontal: '10@ms',
        paddingBottom: '8@ms'
    },
    txtAnswer: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_SMALL * 1.5,
        color: '#314C1C',
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 10,
        marginTop: '15@ms'
    },
    icoDown: {
        width: '10@ms',
        height: '6@ms',
        marginLeft: -5
    }
});

export default AddQuestion;