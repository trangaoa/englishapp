import React, { memo, useEffect, useState } from 'react';
import { FlatListQuestion, Spinner } from '@/components/HOC';
import { useTranslation } from 'react-i18next';
import { DynamicHeader } from '@/components/Header';
import { goBack, navigate } from '@/utils/navigation';
import { useRoute } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { doQuestion, getRandomQuestion } from '@/services';
import { ActivityIndicator, Alert, Image, Text, View } from 'react-native';
import icons from '@/assets/icons';
import { width_screen } from '@/utils';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size } from '@/configs/fonts';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ShuffleQuestion = memo(() => {
    const { t } = useTranslation();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [isLoading, setLoading] = useState(false);
    const [questions, setQuestions] = useState([]);
    const [count, setCount] = useState(0);
    const [data, setData] = useState({
        pageIndex: 1,
        pageSize: 20
    })
    const [solveList, setSolvedList]: any = useState([])

    useEffect(() => {
        getAPI()
    }, [data])

    const getAPI = () => {    
        setLoading(true)
        getRandomQuestion(token, data, (res: any) => {
            console.log('get random', res)
            setLoading(false)
            if (res.code == "00") {
                setQuestions(res?.data?.items);
                setCount(res.data.meta.count)
            }
        })
    }

    const onPrev = () => {
        if (data.pageIndex > 1) {
            setData((preData: any) => {
                let newData = {
                    pageIndex: preData.pageIndex - 1,
                    pageSize: 20
                }
                return newData
            })
        }
    }

    const onNext = () => {
        if (data.pageIndex < count / 20) {
            setData((preData: any) => {
                let newData = {
                    pageIndex: preData.pageIndex + 1,
                    pageSize: 20
                }
                return newData
            })
        }
    }

    const onDoQuestion = () => {
        let arr = solveList ? [...solveList] : [];
        let data = {
            question: arr
        }
        doQuestion(data, token, (res: any) => {
            if (res.code == "00"){
                goBack()
            }
        })
    }

    const onSelectAnswer = (question: any, answer: any) => {
        console.log('question', question, answer);
        let arr = [...solveList]
        arr.push(question.id)
        setSolvedList(arr)
    }

    const onSubmit = () => {
        Alert.alert('Thông báo', 'Bạn có muốn lưu kết quả vừa làm không?',
        [
            {
                text: "Đồng ý",
                onPress: () => onDoQuestion()
            },
            {
                text: "Hủy",
                style: "cancel",
                onPress: () => goBack()
            }
        ])
    }

    return (
        <View style={{ flex: 1 }}>
            <DynamicHeader title={'Câu hỏi ngẫu nhiên'} back onBack={() => onSubmit()}/>
            <View style={{ flex: 1 }}>
                {isLoading ?
                    <Spinner /> :
                    (questions && questions.length > 0 ?
                        <FlatListQuestion
                            data={questions}
                            onRefresh={getAPI}
                            onPressAnswer={(item: any, answer: any) => onSelectAnswer(item, answer)}
                        />
                        : <Text style={{ marginTop: 100, alignSelf: 'center' }}>Chưa có câu hỏi nào</Text>
                    )
                }
            </View>
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderTopWidth: 1, borderTopColor: '#314C1C' }}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => onPrev()}>
                    <Image source={icons.header.arrow_prev} style={styles.icoArrow} />
                </TouchableOpacity>
                <Text style={styles.txtIndex}>{data.pageIndex}<Text style={styles.txtCount}>/{count % 20 == 0 ? count / 20 : Math.round(count / 20) + 1}</Text></Text>
                <TouchableOpacity activeOpacity={0.7} onPress={() => onNext()}>
                    <Image source={icons.header.arrow_next} style={styles.icoArrow} />
                </TouchableOpacity>
            </View>
        </View>
    );
});

export default ShuffleQuestion;

const styles = ScaledSheet.create({
    icoArrow: {
        width: '24@ms',
        height: '24@ms',
        marginHorizontal: '15@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    txtIndex: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_SMALL * 2,
        color: '#314C1C'
    },
    txtCount: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: 'rgba(49, 76, 28, 0.8)'
    }
})