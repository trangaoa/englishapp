import React, { memo, useEffect, useState } from 'react';
import { Text, ScrollView, StatusBar, ActivityIndicator } from 'react-native';
import { Header, Categories } from './components';
import { EmptyState, FlatListPackageHome, FlatListUser, Spinner } from '@/components/HOC';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { height_screen, width_screen } from '@/utils';
import { font, font_size } from '@/configs/fonts';
import { ScaledSheet } from 'react-native-size-matters';
import { useIsFocused } from '@react-navigation/core';
import { getAnotherProfile, getHistory, getLeaderboard, getProfile } from '@/services';
import { setProfile } from '@/redux/ProfileSlice';
import { navigate } from '@/utils/navigation';

const HomePage = memo(() => {
  const { t } = useTranslation();
  const token = useSelector((state: RootState) => state.accessTokenSlice.token);
  const profile: any = useSelector((state: RootState) => state.profileSlice.data);
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const [packages, setPackages] = useState([]);
  const [users, setUsers] = useState([]);
  const [isLoadingPk, setLoadingPk] = useState(false);
  const [isLoadingUser, setLoadingUser] = useState(false);

  let data = {
    pageIndex: 1,
    pageSize: 10
  }
  let userData = {
    week: 1
  }

  useEffect(() => {
    if (isFocused) {
      setLoadingPk(true);
      setLoadingUser(true);
      getAnotherProfile(profile?.id, token, (res: any) => {
        console.log("get profile", res)
        if (res.code == "00") {
          dispatch(setProfile(res.data));
        }
      });
      getHistory(data, token, (res: any) => {
        console.log('getHistory', res)
        setLoadingPk(false)
        if (res.code == "00" && res?.data) {
          setPackages(res.data?.items)
        }
      })
      getLeaderboard(userData, token, (res: any) => {
        setLoadingUser(false)
        if (res.code == "00" && res?.data && res?.data.length > 0) {
          setUsers(res.data)
        }
      })
    }
  }, [isFocused])

  return (
    <>
      <StatusBar barStyle={'dark-content'} />
      <Header
        username={profile?.username}
        avatar={profile?.avatar}
        description={'Welcome to Vatta toeic'}
      />
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        {isLoadingPk ?
          <ActivityIndicator size={'small'} style={{ marginVertical: 20 }} color={'gray'} />
          :
          (packages && packages.length > 0 ?
            <FlatListPackageHome
              header={t('homepage.recent')}
              data={packages}
              seeMore={true}
              onPress={() => navigate('Package')}
            />
            : <></>
          )
        }
        {isLoadingUser ?
          <Spinner />
          : (users && users.length > 0 ?
            <FlatListUser
              header={t('Người dùng tích cực')}
              data={users}
              seeMore={true}
              onPress={() => navigate('Ranking')}
            />
            : <></>
          )
        }
        <Text style={styles.txtHeader}>{'Tiện ích'}</Text>
        <Categories />
      </ScrollView>
    </>
  );
});

const styles = ScaledSheet.create({
  imgBackground: {
    width: width_screen,
    height: height_screen,
    position: 'absolute',
    backgroundColor: '#314C1C',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  },
  container: {
    backgroundColor: '#DAE2D5',
    borderRadius: 20,
    marginTop: -18,
    paddingVertical: '20@ms'
  },
  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: '15@ms'
  },
  txtHeader: {
    fontFamily: font.SFProTextBold,
    fontWeight: '700',
    fontSize: font_size.VERY_LARGE + 2,
    color: '#314C1C',
    marginLeft: '15@ms',
    flex: 1
  },
  txtAll: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.NORMAL,
    color: '#3F6766'

  }
});

export default HomePage;
