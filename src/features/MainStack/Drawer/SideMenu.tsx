import icons from '@/assets/icons';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import { setToken } from '@/redux/AccessTokenSlice';
import { setProfile } from '@/redux/ProfileSlice';
import { height_screen, width_screen } from '@/utils';
import { goBack, navigate, replace } from '@/utils/navigation';
import Storage, { Search } from '@/utils/Storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component, memo } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    FlatList
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ScaledSheet } from 'react-native-size-matters';
import { useDispatch, useSelector } from 'react-redux';

const SideMenu = memo(() => {
    const dispatch = useDispatch();
    const profile: any = useSelector((state: RootState) => state.profileSlice.data);

    let list = [
        {
            icon: icons.side_menu.password,
            name: 'Đổi mật khẩu',
            onPress: () => navigate('ChangePassword'),
            isEnable: true
        },
        {
            icon: icons.side_menu.info,
            name: 'Duyệt câu hỏi',
            onPress: () => navigate('Question'),
            isEnable: profile?.role == 1 ? true : false
        },
        {
            icon: icons.side_menu.account,
            name: 'Tài khoản',
            onPress: () => navigate('Account'),
            isEnable: true
        },
        {
            icon: icons.side_menu.setting,
            name: 'Cài đặt',
            onPress: () => { },
            isEnable: true
        },
        // {
        //     icon: icons.side_menu.language,
        //     name: 'Ngôn ngữ',
        //     onPress: () => { },
        //     isEnable: true
        // },
        {
            icon: icons.side_menu.dark,
            name: 'Chế độ tối',
            onPress: () => { },
            isEnable: true
        },
        {
            icon: icons.side_menu.log_out,
            name: 'Đăng xuất',
            onPress: () => onLogout(),
            isEnable: true
        }
    ]

    const onLogout = async () => {
        await AsyncStorage.setItem('access_token', '');
        Storage.removeData(Search)
        replace('AuthStack');

        dispatch(setToken(''));
        dispatch(setProfile(null));
    }

    return (
        <View style={styles.container}>
            <View style={styles.menu}>
                <TouchableOpacity onPress={() => goBack()}>
                    <Image source={icons.header.back} style={styles.icoBack} />
                </TouchableOpacity>
                <View>
                    <View style={{ alignItems: 'center' }}>
                        {profile?.avatar ?
                            <FastImage
                                source={{
                                    uri: `http://159.223.76.19:3000/api/user/avatar?img=${profile.avatar}`,
                                    priority: FastImage.priority.normal,
                                }}
                                style={styles.imgAvatar}
                            /> :
                            <Image
                                source={icons.other.profile}
                                style={styles.imgAvatar}
                            />
                        }
                        <Text style={styles.txtName}>{profile?.username}</Text>
                        <Text style={styles.txtWelcome}>Welcome to Vatta Toeic</Text>
                        <View style={styles.viewSepe} />
                    </View>
                </View>
                {list.map((item: any, i: any) => {
                    return (
                        item.isEnable ?
                            <TouchableOpacity
                                key={i}
                                style={styles.viewRow}
                                activeOpacity={0.7}
                                onPress={item.onPress}
                            >
                                <Image
                                    source={item.icon}
                                    style={styles.icons}
                                />
                                <Text style={styles.txtOption}>{item.name}</Text>
                            </TouchableOpacity>
                            : <></>
                    );
                })}
            </View>
        </View>
    );
})

export default SideMenu;

export const styles = ScaledSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        width: width_screen,
        height: height_screen,
        padding: 10
    },
    menu: {
        flex: 1,
        backgroundColor: '#FFF',
        position: 'absolute',
        left: 0,
        top: 0,
        width: width_screen * 0.8,
        height: height_screen,
        padding: 10
    },
    menuItem: {
        paddingTop: 10
    },
    icoBack: {
        width: '30@ms',
        height: '30@ms',
        margin: '5@ms'
    },
    imgAvatar: {
        width: '120@ms',
        height: '120@ms',
        borderRadius: '60@ms',
        borderWidth: 2,
        borderColor: '#314C1C'
    },
    txtName: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_SMALL * 2,
        color: '#314C1C',
        marginTop: '10@ms',
        lineHeight: '32@ms'
    },
    txtWelcome: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'rgba(49, 76, 28, 0.65)'
    },
    viewSepe: {
        width: '95%',
        height: 1,
        backgroundColor: '#314C1C',
        marginVertical: '15@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
        marginBottom: 15
    },
    icons: {
        width: '20@ms',
        height: '20@ms'
    },
    txtOption: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        marginLeft: '5@ms'
    }
});