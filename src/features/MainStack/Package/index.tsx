import React, { memo, useEffect, useState } from 'react';
import { FlatListPackage } from '@/components/HOC';
import { DynamicHeader } from '@/components/Header';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { getAllPackage } from '@/services';
import { View } from 'react-native';

const AllPackage = memo(() => {
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [packages, setPackages] = useState([]);

    let data = {
        pageIndex: 1,
        pageSize: 50
    }

    useEffect(() => {
        getAllPackage(data, token, (res: any) => {
            console.log('get all package', res);
            if (res.code == "00" && res?.data) {
                setPackages(res?.data?.items)
            }
        })
    }, [])

    return (
        <View style={{flex: 1}}>
            <DynamicHeader title={'Danh sách bộ câu hỏi'} back />
            <FlatListPackage
                data={packages}
            />
        </View>
    );
});

export default AllPackage;