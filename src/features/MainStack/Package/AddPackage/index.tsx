import { DynamicHeader } from '@/components/Header';
import { Button } from '@/components/HOC';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import React, { memo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, TouchableOpacity, Image, Alert, TextInput } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { useDispatch, useSelector } from 'react-redux';
import { RadioButton, Switch } from 'react-native-paper';
import { useRoute } from '@react-navigation/core';
import { SelectModal } from '@/components/Modal';
import icons from '@/assets/icons';
import { createPackage, createQuestion } from '@/services';
import { navigate } from '@/utils/navigation';
import { setPackages } from '@/redux/PackagesSlice';

const AddPackage = memo(() => {
    const { t } = useTranslation();
    const route: any = useRoute();
    const dispatch = useDispatch();
    const levelRef: any = useRef(null);
    const data = route?.params?.data;
    console.log('route', data)
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const [checked, setChecked] = useState(0);
    const [isHidden, setHidden] = useState(false);
    const [isShuffle, setShuffle] = useState(false);
    const [time, setTime] = useState('0');

    let listLevel = [
        {
            id: 0,
            name: 'Dễ'
        },
        {
            id: 1,
            name: 'Trung bình'
        },
        {
            id: 2,
            name: 'Khó'
        }
    ];
    const [level, setLevel] = useState<any>(listLevel[0]);

    const RenderHeader = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => onCreate()}>
                <Text style={{ color: 'white', paddingVertical: 5, paddingHorizontal: 10, backgroundColor: '#314C1C', borderRadius: 30, marginRight: 15 }}>Tạo</Text>
            </TouchableOpacity>
        )
    }

    const onCreate = () => {
        let answers = [...data?.questions];
        let arr = []
        for (let i = 0; i < answers.length; i++) {
            arr.push(answers[i].id)
        }
        let dataQuestion = {
            time: parseInt(time) * 60,
            name: data?.title,
            status: 0,
            description: data?.description,
            level: level.id + 1,
            hidden: isHidden,
            total: arr.length,
            question: arr
        }
        createPackage(dataQuestion, token, (res: any) => {
            console.log("create ques", res)
            if (res.code == "00") {
                Alert.alert('Thông báo', `Bạn đã tạo thành công bộ đề ${data.title}`);
                dispatch(setPackages([]))
                navigate('MainStack')
            } else {
                Alert.alert('Thông báo', 'Lỗi tạo bộ đề');
            }
        })
    }

    return (
        <>
            <DynamicHeader
                title={'Tạo bộ đề'}
                back
                Header={<RenderHeader />}
            />
            <View style={styles.container}>
                <Text style={styles.txtTitle}>{data?.title}</Text>
                <View style={styles.viewSpace}>
                    <Text style={styles.txtHeader1}>Tổng số câu hỏi</Text>
                    <View style={styles.viewRow}>
                        <Text style={styles.txtHeader}>{data.questions.length}</Text>
                        <Text style={[styles.txtHeader1, { paddingHorizontal: 0 }]}> câu hỏi</Text>
                    </View>
                </View>
                <TouchableOpacity style={styles.viewSpace} activeOpacity={0.7} onPress={() => levelRef.current.open()}>
                    <Text style={styles.txtHeader1}>Thời gian hoàn thành</Text>
                    <View style={styles.viewRow}>
                        <TextInput
                            placeholder={'0'}
                            value={time}
                            onChangeText={setTime}
                            style={styles.txtInput}
                            keyboardType={'numeric'}
                        />
                        <Text style={[styles.txtHeader1, { paddingHorizontal: 0 }]}> phút</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewSpace} activeOpacity={0.7} onPress={() => levelRef.current.open()}>
                    <Text style={styles.txtHeader1}>Độ khó bộ đề</Text>
                    <View style={styles.viewRow}>
                        <Text style={styles.txtHeader1}>{level.name}</Text>
                        <Image
                            source={icons.other.arrowDown}
                            style={styles.icoDown}
                        />
                    </View>
                </TouchableOpacity>
                {/* <Text style={styles.txtHeader}>Chế độ hiển thị</Text> */}
                <View style={styles.viewSpace}>
                    <Text style={styles.txtHeader1}>Hiển thị tên tác giả</Text>
                    <Switch value={!isHidden} onValueChange={setHidden} color={'#314C1C'} children={undefined} onChange={undefined} />
                </View>
                <View style={styles.viewSpace}>
                    <Text style={styles.txtHeader1}>Sắp xếp câu hỏi ngẫu nhiên</Text>
                    <Switch value={isShuffle} onValueChange={setShuffle} color={'#314C1C'} children={undefined} onChange={undefined} />
                </View>
            </View>
            <SelectModal
                modalRef={levelRef}
                data={{
                    title: 'Mức độ khó',
                    options: listLevel
                }}
                onPress={(item: any) => setLevel(item)}
            />
        </>
    );
});

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    txtHeader: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE + 2,
        color: '#314C1C'
    },
    txtHeader1: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        paddingHorizontal: '15@ms'
    },
    txtTitle: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        borderBottomColor: '#314C1C',
        borderBottomWidth: 1,
        paddingHorizontal: '15@ms',
        paddingVertical: '10@ms',
        textAlign: 'center'
    },
    txtAnswer: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_SMALL * 1.5,
        color: '#314C1C',
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 10,
        marginTop: '15@ms'
    },
    icoDown: {
        width: '10@ms',
        height: '6@ms',
        marginLeft: -5
    },
    txtInput: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE + 2,
        lineHeight: '18@ms',
        paddingVertical: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#314C1C',
        textAlign: 'center',
        width: '40@ms'
    }
});

export default AddPackage;