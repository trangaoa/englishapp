import React, { memo, useEffect, useState } from 'react';
import { FlatListAnswerQuestion, FlatListHistoryPackage } from '@/components/HOC';
import { DynamicHeader } from '@/components/Header';
import { navigate } from '@/utils/navigation';
import { useRoute } from '@react-navigation/native';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size } from '@/configs/fonts';
import { getHistoryDetail } from '@/services';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { width_screen } from '@/utils';

const PackageHistoryDetail = memo(() => {
    const route: any = useRoute();
    const [detail, setDetail] = useState(route?.params?.allQuestions);
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);

    useEffect(() => {
        let data = {
            historyId: route?.params?.historyId
        }
        console.log('token', token)
        getHistoryDetail(data, token, (res: any) => {
            console.log('get detail his', res)
            if (res.code == "00") {
                setDetail(res?.data)
            }
        })
    }, [])

    const convertTime = (counter: any) => {
        var minutes: any = Math.floor(counter / 60);
        var seconds: any = counter % 60;

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return minutes + ':' + seconds;
    }

    return (
        <>
            <DynamicHeader title={'Xem lại bài làm'} back />
            <ScrollView style={{ backgroundColor: 'white' }} showsVerticalScrollIndicator={false}>
                <Text style={styles.txtTitle}>{detail?.namePackage}</Text>
                <View style={styles.viewOverview}>
                    <View style={styles.viewRow1}>
                        <Text style={styles.txtLevel}>Điểm số</Text>
                        <Text style={styles.txtPoint}>{Math.round(detail?.point)}</Text>
                    </View>
                    <View style={styles.viewRow1}>
                        <Text style={styles.txtLevel}>Thời gian hoàn thành (phút)</Text>
                        <Text style={styles.txtPoint}>{convertTime(detail?.timePackage)}</Text>
                    </View>
                    <View style={styles.viewRow1}>
                        <Text style={styles.txtLevel}>Thời gian làm bài (phút)</Text>
                        <Text style={styles.txtPoint}>{convertTime(detail?.time)}</Text>
                    </View>
                </View>
                    <FlatListHistoryPackage
                        data={detail?.questions}
                    />
            </ScrollView>
        </>
    );
});

export default PackageHistoryDetail;

const styles = ScaledSheet.create({
    viewRow: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    txtTitle: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.NORMAL,
        color: "#314C1C",
        paddingHorizontal: '15@ms',
        paddingVertical: '10@ms',
        textAlign: 'center',
        alignSelf: 'center',
        borderBottomColor: '#314C1C',
        borderBottomWidth: 1,
        width: width_screen
    },
    viewOverview: {
        borderWidth: 1,
        borderColor: '#314C1C',
        marginVertical: '10@ms',
        marginHorizontal: '15@ms',
        padding: '10@ms',
        borderRadius: '20@ms'
    },
    viewRow1: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    txtLevel: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.NORMAL,
        color: '#314C1C'
    },
    txtPoint: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE + 4,
        lineHeight: '28@ms',
        color: '#2B7D42'
    }
})