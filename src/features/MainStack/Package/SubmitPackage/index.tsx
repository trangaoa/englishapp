import React, { memo, useEffect, useState } from 'react';
import { FlatListAnswerQuestion } from '@/components/HOC';
import { DynamicHeader } from '@/components/Header';
import { navigate } from '@/utils/navigation';
import { useRoute } from '@react-navigation/native';
import { Text, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size } from '@/configs/fonts';
import { width_screen } from '@/utils';

const SubmitPackage = memo(() => {
    const route: any = useRoute();
    const [questions, setQuestions] = useState(route?.params?.allQuestions);
    let submitQuestions = route?.params?.submitQuestions;
    let packageId = route?.params?.packageId;
    const [count, setCount] = useState(0)

    useEffect(() => {
        if (submitQuestions.length > 0) {
            let arr = [...questions]
            submitQuestions.filter((obj: any) => {
                let found = arr.findIndex((item: any) => item.id == obj.id);
                if (found != -1) {
                    arr[found] = {
                        ...arr[found],
                        check: obj.check,
                        answerId: obj.answerId
                    }
                    console.log('true', obj.check)
                    if (obj.check) {
                        setCount((preValue: any) => preValue + 1)
                    }
                }
                setQuestions(arr)
            })
        }
    }, [])

    const onSubmit = () => {
        navigate('PackageDetail', { packageId: packageId })
    }

    return (
        <>
            <DynamicHeader title={'Kết quả bài làm'} back onBack={onSubmit} />
            <Text style={styles.txtTitle}>Điểm số: {Math.round(count / questions.length * 100)}</Text>
            <View style={{ flex: 1 }}>
                <FlatListAnswerQuestion
                    data={questions}
                />
            </View>
        </>
    );
});

export default SubmitPackage;

const styles = ScaledSheet.create({
    txtTitle: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: "#314C1C",
        paddingHorizontal: '15@ms',
        paddingVertical: '10@ms',
        textAlign: 'center',
        alignSelf: 'center',
        borderBottomColor: '#314C1C',
        borderBottomWidth: 0.5,
        width: width_screen,
        backgroundColor: 'white'
    }
})