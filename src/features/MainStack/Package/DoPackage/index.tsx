import React, { memo, useEffect, useState } from 'react';
import { FlatListQuestion } from '@/components/HOC';
import { DynamicHeader } from '@/components/Header';
import { navigate } from '@/utils/navigation';
import { useRoute } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { doPackage } from '@/services';
import { Text, TouchableOpacity, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size } from '@/configs/fonts';

const DoPackage = memo(() => {
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);

    const route: any = useRoute();
    let packageData = route?.params?.package;
    let questionList = packageData.questions;
    const [doData, setDoData]: any = useState([])
    let timeOut = route?.params?.timeOut;
    const [counter, setCounter]: any = useState(timeOut);

    const RenderHeader = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => onSubmit()}>
                <Text style={styles.txtHeader}>Nộp</Text>
            </TouchableOpacity>
        )
    }

    useEffect(() => {
        if (timeOut > 0) {
            if (counter > 0) {
                let interval = setInterval(() => {
                    setCounter((lastTimerCount: any) => {
                        lastTimerCount <= 1 && clearInterval(interval)
                        return lastTimerCount - 1
                    })
                }, 1000)
                return () => clearInterval(interval)
            } else onSubmit();
        }
    }, [counter]);

    const onSelectAnswer = (question: any, answer: any) => {
        let arr = questionList;
        let newArr = [...doData]
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].id == question.id) {
                let found = newArr.findIndex((pre: any) => pre.questionId == question.id)
                if (found == -1) {
                    let obj = {
                        questionId: question.id,
                        answerId: answer.id
                    }
                    newArr.push(obj)
                } else {
                    newArr[found].answerId = answer.id
                }
            }
        }
        setDoData(newArr)
    }

    const onSubmit = () => {
        console.log('submitiing ...')
        let arr = [...doData]
        let data = {
            packageId: packageData.id,
            time: timeOut - counter,
            questions: arr
        }
        doPackage(data, token, (res: any) => {
            if (res.code == "00" && res.data) {
                console.log('after selecting', res)
                navigate('SubmitPackage', { allQuestions: questionList, submitQuestions: res.data, packageId: packageData.id })
            }
        })
    }

    const convertTime = () => {
        // var sec_num = parseInt(counter, 10); // don't forget the second param
        // var hours = Math.floor(sec_num / 3600);
        var minutes: any = Math.floor(counter / 60);
        var seconds: any = counter % 60;

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return minutes + ':' + seconds;
    }

    return (
        <View style={{ flex: 1 }}>
            <DynamicHeader title={'Làm bộ đề'} back Header={<RenderHeader />} />
            <View style={{ flex: 1 }}>
                <FlatListQuestion
                    data={questionList}
                    isPressAnswer={true}
                    onPressAnswer={(item: any, answer: any) => onSelectAnswer(item, answer)}
                />
            </View>
            {counter > 0 ?
                <View style={styles.viewBottom}>
                    <Text style={styles.txtCount}>{convertTime()}</Text>
                </View>
                : <></>
            }
        </View>
    );
});

export default DoPackage;

const styles = ScaledSheet.create({
    icoArrow: {
        width: '24@ms',
        height: '24@ms',
        marginHorizontal: '15@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    txtCount: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE * 1.5,
        alignSelf: 'center',
        color: 'rgba(49, 76, 28, 0.8)'
    },
    viewBottom: {
        height: '50@ms',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: '#314C1C'
    },
    txtHeader: {
        color: 'white',
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#314C1C',
        borderRadius: 30,
        marginRight: 15
    }
})