import icons from '@/assets/icons';
import { DynamicHeader } from '@/components/Header';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import { setPackages } from '@/redux/PackagesSlice';
import { goBack, navigate } from '@/utils/navigation';
import { useIsFocused, useRoute } from '@react-navigation/core';
import React, { memo, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, TouchableOpacity, TextInput, Image, Alert, ScrollView } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { useDispatch, useSelector } from 'react-redux';

const CreatePackage = memo(() => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const dispatch = useDispatch();
    const [value, setValue] = useState('');
    const [title, setTitle] = useState('');
    const packages: any = useSelector((state: RootState) => state.packagesSlice.data);
    const [questionList, setQuestionList]: any = useState(packages && packages.length > 0 ? packages : []);

    useEffect(() => {
        if (isFocused) {
            setQuestionList(packages)
        }
    }, [isFocused])

    const RenderHeader = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => onNext()}>
                <Text style={{ color: 'white', paddingVertical: 5, paddingHorizontal: 10, backgroundColor: '#314C1C', borderRadius: 30, marginRight: 15 }}>Tiếp</Text>
            </TouchableOpacity>
        )
    }

    const onNext = () => {
        if (title != '') {
            if (questionList.length > 1 && questionList.length < 21) {
                let data = {
                    title: title,
                    description: value,
                    questions: questionList
                }
                navigate('AddPackage', { data: data })
            } else Alert.alert('Thông báo', 'Mỗi bộ đề có từ 2 đến 20 câu hỏi')
        } else {
            Alert.alert('Thông báo', 'Vui lòng điền tiêu đề bộ câu hỏi')
        }
    }

    const onBack = () => {
        Alert.alert('Thông báo', 'Bạn đang tạo bộ đề. Nếu bạn ấn đồng ý, bộ đề này sẽ bị hủy',
            [
                {
                    text: "Đồng ý",
                    onPress: () => {
                        dispatch(setPackages([]))
                        goBack()
                    }
                },
                {
                    text: "Hủy",
                    style: "cancel",
                    onPress: () => { }
                }
            ])
    }

    const onDeleteQuestion = (item: any) => {
        let newData = [...questionList]
        for (let i = 0; i <  newData.length; i++){
            if (newData[i].id == item.id){
                newData.splice(i, 1)
            }
        }
        setQuestionList(newData)
        dispatch(setPackages(newData))
    }

    return (
        <>
            <DynamicHeader
                title={'Tạo bộ đề'}
                back
                onBack={() => onBack()}
                Header={<RenderHeader />}
            />
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <View style={styles.viewRow}>
                    <Text style={[styles.txtHeader, { marginTop: 0 }]}>Tiêu đề: </Text>
                    <TextInput
                        placeholder={''}
                        style={styles.txtInput}
                        value={title}
                        onChangeText={setTitle}
                    />
                </View>
                <Text style={styles.txtHeader}>Mô tả:</Text>
                <TextInput
                    style={styles.txtInputArea}
                    value={value}
                    editable
                    onChangeText={(text: any) => setValue(text)}
                    multiline={true}
                    placeholder={'Vui lòng nhập mô tả bộ đề'}
                />
                <Text style={styles.txtNote}>Một bộ đề có từ 2 đến 20 câu hỏi</Text>
                {
                    questionList && questionList.length > 0 ?
                        questionList.map((item: any, index: any) => {
                            return (
                                <RenderQuestion item={item} index={index} onPress={onDeleteQuestion} />
                            )
                        })
                        : null
                }
                <TouchableOpacity activeOpacity={0.7} style={styles.viewAdd} onPress={() => navigate('AllQuestion')}>
                    <Image source={icons.package.add} style={styles.icoAdd} />
                    <Text style={styles.txtAdd}>Thêm câu hỏi</Text>
                </TouchableOpacity>
                <View style={{ height: 20 }} />
            </ScrollView>
        </>
    );
});

const RenderQuestion = memo(({ item, index, onPress }: any) => {
    return (
        <View style={styles.viewItem} key={index}>
            <View style={styles.viewNumber}>
                <View style={[styles.viewRow, { justifyContent: 'space-between' }]}>
                    <View style={[styles.viewRow, {flex: 1}]}>
                        <Text style={styles.txtNumber} numberOfLines={1}>{`Câu hỏi ${item?.id}:`}</Text>
                        <Text style={[styles.txtCorrect, { color: item.level == 1 ? '#54A019' : item.level == 2 ? '#CC8A26' : '#BC2525' }]} numberOfLines={1}>{item.level == 1 ? 'Dễ' : item.level == 2 ? 'Trung bình' : 'Khó'}</Text>
                    </View>
                    <TouchableOpacity activeOpacity={0.7} onPress={() => onPress(item)}>
                        <Image source={icons.other.close}
                            style={styles.icoClose}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <Text style={styles.txtQuestion}>{item?.title}</Text>
        </View>
    )
});

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: '15@ms',
        paddingTop: '15@ms'
    },
    txtHeader: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        marginTop: '10@ms'
    },
    txtInputArea: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        borderColor: '#314C1C',
        borderWidth: 0.5,
        borderRadius: 10,
        height: '120@ms',
        textAlignVertical: 'top',
        padding: '10@ms'
    },
    txtInput: {
        paddingVertical: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#314C1C',
        flex: 1
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtNote: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_SMALL,
        color: 'rgba(0, 0, 0, 0.5)',
        marginTop: '10@ms'
    },
    viewAdd: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderWidth: 2,
        borderColor: '#314C1C',
        paddingHorizontal: '15@ms',
        paddingVertical: '8@ms',
        marginVertical: '15@ms',
        borderRadius: 10
    },
    icoAdd: {
        width: '20@ms',
        height: '20@ms',
        marginRight: '10@ms'
    },
    txtAdd: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C'
    },
    viewItem: {
        borderRadius: '10@ms',
        borderWidth: 0.5,
        borderColor: '#314C1C',
        marginTop: '15@ms'
    },
    viewNumber: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    txtNumber: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        backgroundColor: '#314C1C',
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingVertical: 5,
        paddingHorizontal: '20@ms'
    },
    txtCorrect: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#54A019',
        marginVertical: '5@ms',
        marginLeft: '10@ms'
    },
    txtQuestion: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        marginVertical: '5@ms',
        marginHorizontal: '10@ms'
    },
    icoClose: {
        width: '12@ms',
        height: '12@ms',
        marginHorizontal: '10@ms'
    }
});

export default CreatePackage;