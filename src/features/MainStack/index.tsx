import React, { memo } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();
import HomePage from './HomePage';
import Booking from './Dictionary';
import Vaccine from './Ranking';
import Account from './Account';
import Loyalty from './Loyalty';
import { useTranslation } from 'react-i18next';
import { ScaledSheet } from 'react-native-size-matters';
import { CustomTabBar } from './CustomTabBar';
import icons from '@/assets/icons';
import { Image, View } from 'react-native';
import { height_screen, width_screen } from '@/utils';

const MainStack = memo(function () {
  const { t } = useTranslation();

  return (
    <View style={{ width: width_screen, height: height_screen }}>
      <Tab.Navigator
        tabBar={(props) => <CustomTabBar {...props} />}
      >
        <Tab.Screen
          name='HomePage'
          component={HomePage}
          options={{
            title: t('tabbar.home'),
            tabBarIcon: ({ focused }) => (
              <Image
                style={[styles.ico, { tintColor: focused ? '#314C1C' : '#8C8C8C' }]}
                source={icons.tabbar.home}
              />
            ),
          }}
        />
        <Tab.Screen
          name='Dictionary'
          component={Booking}
          options={{
            title: 'Từ điển',
            tabBarIcon: ({ focused }) => (
              <Image
                style={[styles.ico, { tintColor: focused ? '#314C1C' : '#8C8C8C' }]}
                source={icons.tabbar.booking}
              />
            ),
          }}
        />
        <Tab.Screen
          name='Loyalty'
          component={Loyalty}
        />
        <Tab.Screen
          name='Ranking'
          component={Vaccine}
          options={{
            title: 'BXH',
            tabBarIcon: ({ focused }) => (
              <Image
                style={[styles.ico, { tintColor: focused ? '#314C1C' : '#8C8C8C' }]}
                source={icons.tabbar.messages}
              />
            ),
          }}
        />
        <Tab.Screen
          name='Account'
          component={Account}
          options={{
            title: t('tabbar.account'),
            tabBarIcon: ({ focused }) => (
              <Image
                style={[styles.ico, { tintColor: focused ? '#314C1C' : '#8C8C8C' }]}
                source={icons.tabbar.account}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </View>
  );
})

export default MainStack;

const styles = ScaledSheet.create({
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ico: {
    width: '24@ms',
    height: '24@ms'
  }
})
