import React, { memo, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, TextInput, Image } from 'react-native';
import { useTranslation } from 'react-i18next';
import { DynamicHeader, SearchHeader } from '@/components/Header';
import { font, font_size } from '@/configs/fonts';
import { ScaledSheet } from 'react-native-size-matters';
import { navigate } from '@/utils/navigation';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { suggestKeyword } from '@/services';
import { height_screen, width_screen } from '@/utils';
import icons from '@/assets/icons';
import Storage, { Search } from '@/utils/Storage';
import { useIsFocused } from '@react-navigation/native';

const Booking = memo(() => {
  const { t } = useTranslation();
  const isFocused = useIsFocused();
  const [onFocus, setFocus] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [suggest, setSuggest] = useState([]);
  let historyData = [{ english: "abhorrence" }]
  const [history, setHistory] = useState(historyData);
  const token = useSelector((state: RootState) => state.accessTokenSlice.token);

  useEffect(() => {
    suggestKeyword(keyword, token, (res: any) => {
      // setSuggest()
      console.log('suggest', res)
      if (res.code == "00") {
        setSuggest(res.data)
      }
    })
  }, [keyword])

  useEffect(() => {
    if (isFocused) {
      setTimeout(async () => {
        let search = await Storage.getObject(Search);
        if (search) {
          setHistory(search)
        }
        console.log('search', search)
      }, 800);
    }
  }, [isFocused])

  const onAdd = (word: any) => {
    let preData = [...history];
    let found = preData.findIndex((obj: any) => obj.english == word.english);
    if (found != -1){
      preData.splice(found, 1);
    } 
    preData.unshift(word);
    Storage.setData(Search, preData)
    navigate('Definition', { word: word.english })
  }

  const onDel = () => {
    Storage.removeData(Search)
    setHistory([])
  }

  return (
    <View style={styles.container}>
      <View style={{ height: 50, marginTop: 70 }}>
        <View style={[styles.viewInput, { borderColor: onFocus ? '#3F6766' : '#E3E5E6' }]}>
          <Image
            source={icons.header.search}
            style={styles.icon}
          />
          <TextInput
            placeholder={'Nhập từ khóa'}
            style={styles.txtInput}
            onFocus={() => setFocus(true)}
            onBlur={() => { setFocus(false); setSuggest([]) }}
            value={keyword}
            onChangeText={setKeyword}
          />
        </View>
      </View>
      <View style={{ flex: 1 }}>
        <View style={styles.viewRow}>
        <Text style={styles.txtHistory}>Lịch sử tìm kiếm</Text>
        <TouchableOpacity onPress={onDel}><Text style={styles.txtDel}>Xóa</Text></TouchableOpacity>
        </View>
        {
          history && history.length > 0 ?
            <FlatList
              data={history}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) => (<RenderHistory item={item} index={index} onChoose={onAdd}/>)}
            />
            : <Text style={styles.txtHello}>Chưa có lịch sử tìm kiếm</Text>
        }
        {
          suggest && suggest.length > 0 ?
            <View style={styles.viewSuggest}>
              <FlatList
                data={suggest}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (<RenderItem item={item} index={index}  onChoose={onAdd}/>)}
              />
            </View>
            : <></>
        }
      </View>
    </View>
  );
});

const RenderHistory = memo(({ item, index, onChoose }: { item: any, index: any, onChoose: any }) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={() => onChoose(item)}>
      <Text style={styles.txtHello}>{item.english}</Text>
    </TouchableOpacity>
  )
});

const RenderItem = memo(({ item, index, onChoose }: { item: any, index: any, onChoose: any }) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={() => onChoose(item)}>
      <Text style={styles.txtEnglish}>{item.english}</Text>
    </TouchableOpacity>
  )
});

const styles = ScaledSheet.create({
  container: {
    backgroundColor: 'rgba(218, 226, 213, 1)',
    flex: 1,
    paddingHorizontal: '25@ms'
  },
  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 30
  },
  txtHistory: {
    fontFamily: font.SFProTextMedium,
    fontSize: font_size.VERY_LARGE + 4,
    color: '#314C1C'
  },
  txtDel: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    color: '#314C1C'
  },
  viewSuggest: {
    backgroundColor: 'white',
    height: height_screen * 0.5,
    width: width_screen * 0.8,
    alignSelf: 'center',
    borderRadius: 10,
    position: 'absolute',
    top: 10
  },
  txtEnglish: {
    fontFamily: font.SFProTextMedium,
    fontSize: font_size.VERY_LARGE,
    color: 'black',
    borderBottomColor: '#314C1C',
    borderBottomWidth: 0.25,
    paddingVertical: 5,
    paddingHorizontal: 20
  },
  txtHello: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    color: '#314C1C',
    paddingVertical: 5,
    paddingHorizontal: 20
  },
  viewInput: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: '8@ms',
    alignItems: 'center',
    backgroundColor: 'white',
    height: '40@ms',
    flex: 1
  },
  icon: {
    width: '24@ms',
    height: '24@ms',
    paddingVertical: 0,
    marginLeft: '10@ms',
    marginRight: '5@ms',
    tintColor: '#314C1C'
  },
  txtInput: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    color: '#090A0A',
    paddingVertical: 0,
    flex: 1
  }
});

export default Booking;