import React, { memo, useState, useEffect } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useRoute } from '@react-navigation/native';
import { goBack } from '@/utils/navigation';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size } from '@/configs/fonts';
import { findKeyword } from '@/services';
import icons from '@/assets/icons';

const QuestionDetail = memo(() => {
    const { t } = useTranslation();
    const route = useRoute<any>();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    let word = route?.params?.word;
    const [detail, setDetail]: any = useState();

    useEffect(() => {
        findKeyword(word, token, (res: any) => {
            console.log('find ', res)
            if (res.code == "00") {
                setDetail(res.data)
            }
        })
    }, [])

    return (
        <>
            <View style={styles.containerTop}>
                <View style={styles.viewRow}>
                    <TouchableOpacity onPress={() => goBack()}>
                        <Image
                            source={icons.header.white_back}
                            style={styles.icoBack}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => goBack()}>
                        <Image
                            source={icons.header.speaker}
                            style={styles.icoSpeaker}
                        />
                    </TouchableOpacity>
                </View>
                <Text style={styles.txtWord}>{word}</Text>
                <Text style={styles.txtPron}>{detail?.pronunciation}</Text>
            </View>
            <View style={styles.viewDef}>
                {detail?.type ? <Text style={styles.txtType}>{detail?.type}</Text> : <></>}
                <Text style={styles.txtDef}>Định nghĩa</Text>
                {
                    detail?.vietnamese && detail?.vietnamese.length > 0 ?
                        detail?.vietnamese.map((item: any, index: any) => {
                            return (
                                <Text style={styles.txtExplain}>{index + 1}. {item}</Text>
                            )
                        })
                        : <Text style={styles.txtExplain}>Chưa có định nghĩa cho từ này</Text>
                }
            </View>
        </>
    );
});

export default QuestionDetail;

const styles = ScaledSheet.create({
    container: {
        // marginBottom: '20@ms'
    },
    viewAccount: {
        backgroundColor: '#314C1C',
        borderBottomRightRadius: '20@ms',
        borderBottomLeftRadius: '20@ms',
        paddingHorizontal: '40@ms',
        paddingVertical: '20@ms',
        alignItems: 'center'
    },
    txtWord: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE * 2,
        color: 'white',
        textAlign: 'center'
    },
    containerTop: {
        backgroundColor: '#314C1C',
        height: '170@ms',
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40
    },
    icoBack: {
        width: '40@ms',
        height: '40@ms'
    },
    icoSpeaker: {
        width: '30@ms',
        height: '30@ms'
    },
    txtPron: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: 'white',
        alignSelf: 'center'
    },
    viewRow: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 30,
        marginHorizontal: 10
    },
    viewDef: {
        marginHorizontal: '20@ms',
        marginTop: 20
    },
    txtType: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: 'black',
        marginBottom: 10
    },
    txtDef: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE + 4,
        color: '#314C1C',
    },
    txtExplain: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: 'black',
    }
});