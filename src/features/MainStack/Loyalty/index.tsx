import icons from '@/assets/icons';
import { DynamicHeader } from '@/components/Header';
import { EmptyState, Spinner } from '@/components/HOC';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import { getStories } from '@/services';
import { navigate } from '@/utils/navigation';
import React, { memo, useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { ScaledSheet } from 'react-native-size-matters';
import { useSelector } from 'react-redux';

const Loyalty = memo(() => {
  const token = useSelector((state: RootState) => state.accessTokenSlice.token);
  const [list, setList] = useState([]);
  const [isLoading, setLoading] = useState(false);

  let data = {
    pageIndex: 1,
    pageSize: 20
  }

  useEffect(() => {
    setLoading(true)
    getStories(data, token, (res: any) => {
      setLoading(false)
      console.log('get list story', res)
      if (res.code == '00' && res?.data && res?.data?.meta.count > 0) {
        setList(res?.data?.items)
      }
    })
  }, [])

  return (
    <View style={styles.container}>
      <DynamicHeader title={'Truyện nói Tiếng anh'} />
      {
        isLoading ?
          <Spinner /> :
          <FlatList
            data={list}
            numColumns={2}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (<RenderItem item={item} index={index} />)}
            style={{ marginHorizontal: 10 }}
            ListEmptyComponent={<EmptyState content={'Chưa có truyện nào'}/>}
          />
      }
    </View>
  );
});

const RenderItem = memo(({ item, index }: { item: any, index: any }) => {
  return (
    <TouchableOpacity style={styles.containerItem} key={index} activeOpacity={0.7} onPress={() => navigate('PlaySound', { item: item })}>
      {item?.background ?
        <FastImage
          source={{
            uri: `http://159.223.76.19:3000/api/user/avatar?img=${item.background}`,
            priority: FastImage.priority.normal,
          }}
          style={styles.imgAvatar}
        /> :
        <Image
          source={icons.other.profile}
          style={styles.imgAvatar}
        />
      }
      <Text style={styles.txtTitle}>{item?.title}{item?.title}</Text>
    </TouchableOpacity>
  )
});

export default Loyalty;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  imgAvatar: {
    width: '150@ms',
    height: "150@ms"
  },
  containerItem: {
    maxWidth: '50%',
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: '20@ms'
  },
  txtTitle: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.NORMAL,
    textAlign: 'center',
    marginTop: 5
  }
});