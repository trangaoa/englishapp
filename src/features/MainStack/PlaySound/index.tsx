import React, { memo, useEffect, useState } from 'react'
import { View, Image, Text, TouchableOpacity, Platform, Alert, Slider, ScrollView, ImageBackground } from 'react-native';

// import Sound from 'react-native-sound';

import { useIsFocused, useRoute } from '@react-navigation/native';
import icons from '@/assets/icons';
import { ScaledSheet } from 'react-native-size-matters';
import Sound from 'react-native-sound';
import { DynamicHeader } from '@/components/Header';
import FastImage from 'react-native-fast-image';
import { font, font_size } from '@/configs/fonts';
import { width_screen } from '@/utils';
import { goBack } from '@/utils/navigation';

const PlayerScreen = memo(() => {
    const route: any = useRoute();
    const isFocused = useIsFocused();
    let item = route?.params?.item;
    let title = item?.title;
    const [playState, setPlayState] = useState('playing');
    const [playSeconds, setPlaySeconds] = useState(0);
    const [duration, setDuration] = useState(0);
    const [sliderEditing, setSlider] = useState(false);
    const [whoosh, setWhoosh]: any = useState();

    const filepath = `http://159.223.76.19:3000/api/user/avatar?img=${item?.audio}`;

    useEffect(() => {
        if (isFocused) {
            play();
        }
    }, [isFocused])

    useEffect(() => {
        console.log('whoosh', whoosh, playState,'sliderEditing', sliderEditing)
        let timeout = setInterval(() => {
            if (whoosh && whoosh.isLoaded() && !sliderEditing) {
                whoosh.getCurrentTime((seconds: any, isPlaying: any) => {
                    setPlaySeconds(seconds)
                })
            }
        }, 100);
        return () => clearInterval(timeout);
    }, [whoosh])

    const play = () => {
        // Sound.setCategory('Playback');
        var sound = new Sound(filepath, '', (error: any) => {
            if (error) {
                Alert.alert('Thông báo', 'Lỗi tải file audio');
                setPlayState('paused')
            } else {
                setPlayState('playing');
                setDuration(sound.getDuration());

                sound?.setCurrentTime(playSeconds);

                sound.play((success: any) => {
                    if (success) {
                        console.log('Phát nhạc hoàn thành');
                        setPlayState('stopped')
                    } else {
                        Alert.alert('Chú ý', 'Lỗi file audio');
                        setPlayState('paused')
                        setPlaySeconds(0)
                        sound.setCurrentTime(0);
                        // whoosh.release()
                    }
                });
            }
        });
        sound.setVolume(0.7);
        sound.setNumberOfLoops(0);
        setWhoosh(sound);
    }

    const onSliderEditStart = () => {
        setSlider(true);
    }
    const onSliderEditEnd = () => {
        setSlider(false);
    }
    const onSliderEditing = (value: number) => {
        if (whoosh) {
            console.log('value time', value)
            whoosh?.setCurrentTime(value);
            setPlaySeconds(value)
        }
    }

    const playAgain = () => {
        var sound = new Sound(filepath, '', (error: any) => {
            if (error) {
                Alert.alert('Thông báo', 'Lỗi tải file audio');
                setPlayState('paused')
            } else {
                setPlayState('playing');
                setDuration(sound.getDuration());

                sound.play((success: any) => {
                    if (success) {
                        console.log('Phát nhạc hoàn thành');
                        setPlayState('stopped')
                    } else {
                        Alert.alert('Chú ý', 'Lỗi file audio');
                        setPlayState('paused')
                        setPlaySeconds(0)
                        sound.setCurrentTime(0);
                        // whoosh.release()
                    }
                });
            }
        });
        sound.setVolume(0.7);
        sound.setNumberOfLoops(5);
        setWhoosh(sound);
    }

    const pause = () => {
        if (whoosh) {
            whoosh?.pause();
        }
        setPlayState('paused')
    }

    const jumpPrev15Seconds = () => { jumpSeconds(-15); }
    const jumpNext15Seconds = () => { jumpSeconds(15); }
    const jumpSeconds = (secsDelta: any) => {
        if (whoosh) {
            whoosh?.getCurrentTime((secs: any, isPlaying: any) => {
                let nextSecs = secs + secsDelta;
                if (nextSecs < 0) nextSecs = 0;
                else if (nextSecs > duration) nextSecs = duration;
                whoosh?.setCurrentTime(nextSecs);
                setPlaySeconds(nextSecs)
            })
        }
    }

    const getAudioTimeString = (seconds: number) => {
        // const h = Math.round(seconds / (60 * 60));
        const m = Math.round(seconds % (60 * 60) / 60);
        const s = Math.round(seconds % 60);

        return ((m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s));
        // return ((h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s));
    }

    const durationString = getAudioTimeString(duration);

    return (
        <>
            <View style={styles.container}>
                <DynamicHeader title={title.length > 20 ? `${title.slice(0, 20)}...` : title} back onBack={() => { pause(); goBack() }} />
                {item?.background ?
                    <FastImage
                        source={{
                            uri: `http://159.223.76.19:3000/api/user/avatar?img=${item.background}`
                        }}
                        style={styles.imgBg1}
                    />
                    :
                    <Image
                        source={icons.other.profile}
                        style={styles.imgBg1}
                    />
                }
                {item?.background ?
                    <ImageBackground
                        source={{
                            uri: `http://159.223.76.19:3000/api/user/avatar?img=${item.background}`
                        }}
                        style={styles.imgBg}
                    >
                        <ScrollView showsVerticalScrollIndicator={false} style={styles.viewContent}>
                            <Text style={styles.txtContent}>{item?.content ? item.content : 'Chưa có nội dung của truyện này'}</Text>
                        </ScrollView>
                    </ImageBackground>
                    :
                    <Image
                        source={icons.other.profile}
                        style={styles.imgBg}
                    />
                }
                <View style={styles.viewBtn}>
                    <TouchableOpacity onPress={jumpPrev15Seconds} style={{ justifyContent: 'center' }}>
                        <Image source={icons.sound.prev} style={styles.icoPlay} />
                    </TouchableOpacity>
                    {playState == 'playing' &&
                        <TouchableOpacity onPress={pause} style={{ marginHorizontal: 20 }}>
                            <Image source={icons.sound.pause} style={styles.icoPlay} />
                        </TouchableOpacity>}
                    {playState == 'paused' &&
                        <TouchableOpacity onPress={play} style={{ marginHorizontal: 20 }}>
                            <Image source={icons.sound.play} style={styles.icoPlay} />
                        </TouchableOpacity>}
                    {playState == 'stopped' &&
                        <TouchableOpacity onPress={playAgain} style={{ marginHorizontal: 20 }}>
                            <Image source={icons.sound.reset} style={styles.icoPlay} />
                        </TouchableOpacity>}
                    <TouchableOpacity onPress={jumpNext15Seconds} style={{ justifyContent: 'center' }}>
                        <Image source={icons.sound.next} style={styles.icoPlay} />
                    </TouchableOpacity>
                </View>
                <View style={styles.viewSlider}>
                    <Text style={styles.txtTime}>{getAudioTimeString(playSeconds)}</Text>
                    <Slider
                        onTouchStart={onSliderEditStart}
                        onTouchEnd={onSliderEditEnd}
                        onValueChange={(value: any) => onSliderEditing(value)}
                        value={playSeconds}
                        maximumValue={duration}
                        minimumValue={0}
                        maximumTrackTintColor='gray'
                        minimumTrackTintColor='#314C1C'
                        thumbTintColor='#314C1C'
                        style={{ flex: 1, alignSelf: 'center' }} />
                    <Text style={styles.txtTime}>{durationString}</Text>
                </View>
            </View>
        </>
    )
});

export default PlayerScreen;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DAE2D5'
    },
    imgBg: {
        width: width_screen * 0.9,
        height: '400@ms',
        resizeMode: 'cover',
        marginVertical: '20@ms',
        alignSelf: 'center'
    },
    imgBg1: {
        width: '60@ms',
        height: '60@ms',
        marginTop: '20@ms',
        alignSelf: 'center'
    },
    txtTime: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        alignSelf: 'center'
    },
    icoPlay: {
        width: '30@ms',
        height: '30@ms',
        tintColor: '#314C1C'
    },
    viewBtn: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 10
    },
    viewSlider: {
        marginVertical: 20,
        marginHorizontal: 15,
        flexDirection: 'row'
    },
    viewContent: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        width: '100%',
        flex: 1
    },
    txtContent: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE + 4,
        textAlign: 'center',
        color: 'white',
        lineHeight: '36@ms',
        marginHorizontal: 25,
        marginVertical: 15
    }
})