import icons from '@/assets/icons';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import { getLeaderboard } from '@/services';
import React, { memo, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import FastImage from 'react-native-fast-image';
import { ScaledSheet } from 'react-native-size-matters';
import { useSelector } from 'react-redux';

const Ranking = memo(() => {
    const [index, setIndex] = useState(1);
    const [ranking, setRanking]: any = useState([]);
    const [isLoadingUser, setLoadingUser] = useState(false);
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);

    let list = [
        { id: 0, type: 'Top follow' },
        { id: 1, type: 'Top câu hỏi' },
        { id: 2, type: 'Điểm cao' }
    ]

    let userData = {
        week: 1
    }

    useEffect(() => {
        setLoadingUser(true);
        getLeaderboard(userData, token, (res: any) => {
            setLoadingUser(false)
            console.log('get leader in home', res);
            if (res.code == "00" && res?.data && res?.data.length > 0) {
                setRanking(res.data)
            }
        })
    }, [])

    return (
        <View style={{ flex: 1, backgroundColor: '#DAE2D5' }}>
            <View style={styles.viewTop}>
                <Text style={styles.txtTitle}>Bảng xếp hạng</Text>
                <View style={styles.viewTab}>
                    {list.map((item: any, i: any) => {
                        return (
                            <TouchableOpacity
                                key={i}
                                activeOpacity={0.7}
                                onPress={() => setIndex(i)}>
                                <Text style={index == i ? styles.txtActive : styles.txtInactive}>{item.type}</Text>
                            </TouchableOpacity>
                        );
                    })}
                </View>
                <Image
                    source={icons.rank.crown}
                    style={styles.icoCrown}
                />
                {ranking && ranking.length > 0 ?
                    <View style={styles.viewTab}>
                        {ranking.length > 1 ?
                            <TouchableOpacity style={styles.viewUser2}>
                                {ranking[1]?.avatar ?
                                    <FastImage
                                        source={{
                                            uri: `http://159.223.76.19:3000/api/user/avatar?img=${ranking[1]?.avatar}`,
                                            priority: FastImage.priority.normal,
                                        }}
                                        style={styles.avatar2}
                                    /> :
                                    <Image
                                        source={icons.other.profile}
                                        style={styles.avatar2}
                                    />
                                }
                                <Text style={styles.txtName2}>{ranking[1].username}</Text>
                                <Text style={styles.txtScore2}>{Math.round(ranking[1].point)}</Text>
                            </TouchableOpacity>
                            : <TouchableOpacity style={styles.viewUser2}></TouchableOpacity>
                        }
                        <TouchableOpacity style={styles.viewUser1}>
                            {ranking[0]?.avatar ?
                                <FastImage
                                    source={{
                                        uri: `http://159.223.76.19:3000/api/user/avatar?img=${ranking[0]?.avatar}`,
                                        priority: FastImage.priority.normal,
                                    }}
                                    style={styles.avatar1}
                                /> :
                                <Image
                                    source={icons.other.profile}
                                    style={styles.avatar1}
                                />
                            }
                            <Text style={styles.txtName1}>{ranking[0].username}</Text>
                            <Text style={styles.txtScore1}>{Math.round(ranking[0].point)}</Text>
                        </TouchableOpacity>
                        {ranking.length > 2 ?
                            <TouchableOpacity style={styles.viewUser2}>
                                {ranking[2]?.avatar ?
                                    <FastImage
                                        source={{
                                            uri: `http://159.223.76.19:3000/api/user/avatar?img=${ranking[2]?.avatar}`,
                                            priority: FastImage.priority.normal,
                                        }}
                                        style={styles.avatar2}
                                    /> :
                                    <Image
                                        source={icons.other.profile}
                                        style={styles.avatar2}
                                    />
                                }
                                <Text style={styles.txtName2}>{ranking[2].username}</Text>
                                <Text style={styles.txtScore2}>{Math.round(ranking[2].point)}</Text>
                            </TouchableOpacity>
                            : <TouchableOpacity style={styles.viewUser2}></TouchableOpacity>
                        }
                    </View>
                    : <></>
                }
            </View>
            {ranking.length > 3 ?
                <FlatList
                    data={ranking.splice(0, 3)}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (<RenderItem item={item} index={index} />)}
                    ListFooterComponent={<View style={styles.footer} />}
                    style={{ marginHorizontal: 20 }}
                    showsVerticalScrollIndicator={false}
                />
                : <></>
            }
        </View>
    );
});

const RenderItem = ({ item, index }: {
    item: any, index: any
}) => {
    return (
        <TouchableOpacity activeOpacity={0.7} style={styles.containerItem} key={index}>
            <View style={styles.viewRank}>
                <Text style={styles.txtRank}>{`${index + 4}`}</Text>
                <Image
                    source={icons.account.up}
                    style={styles.icoUp}
                />
            </View>
            <View style={styles.viewUsername}>
                {item?.avatar ?
                    <FastImage
                        source={{
                            uri: `http://159.223.76.19:3000/api/user/avatar?img=${item?.avatar}`,
                            priority: FastImage.priority.normal,
                        }}
                        style={styles.otherAvatar}
                    /> :
                    <Image
                        source={icons.other.profile}
                        style={styles.otherAvatar}
                    />
                }
                <Text style={styles.txtOtherName}>{item.username}</Text>
                <Text style={styles.txtOtherScore}>{Math.round(item.point)}</Text>
            </View>
        </TouchableOpacity>
    )
};

export default Ranking;

const styles = ScaledSheet.create({
    container: {
        // marginBottom: '20@ms'
    },
    viewTop: {
        backgroundColor: '#314C1C',
        height: '350@ms',
        borderBottomLeftRadius: '30@ms',
        borderBottomRightRadius: '30@ms'
    },
    txtTitle: {
        fontFamily: font.SFProTextBold,
        fontSize: font_size.VERY_LARGE + 4,
        color: 'white',
        alignSelf: 'center',
        marginTop: '10@ms',
        marginBottom: '20@ms'
    },
    viewTab: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: '30@ms',
        justifyContent: 'space-between'
    },
    txtActive: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        backgroundColor: 'white',
        paddingVertical: '6@ms',
        borderRadius: '30@ms',
        paddingHorizontal: '15@ms'
    },
    txtInactive: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#5A833A',
        paddingHorizontal: '15@ms'
    },
    icoCrown: {
        width: '30@ms',
        height: '30@ms',
        alignSelf: 'center',
        marginVertical: '8@ms'
    },
    viewUser1: {
        alignItems: 'center',
        flex: 1.2
    },
    txtName1: {
        fontFamily: font.SFProTextMedium,
        fontSize: font_size.VERY_LARGE,
        color: 'white',
        marginTop: '10@ms',
        marginBottom: '10@ms'
    },
    txtScore1: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_SMALL * 3,
        lineHeight: '42@ms',
        color: 'white'
    },
    avatar1: {
        width: '90@ms',
        height: '90@ms',
        borderRadius: '45@ms',
        borderWidth: 2,
        borderColor: 'white'
    },
    viewUser2: {
        alignItems: 'center',
        flex: 0.9
    },
    txtName2: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        marginTop: '10@ms',
        marginBottom: '15@ms',
        opacity: 0.9
    },
    txtScore2: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.NORMAL * 2,
        lineHeight: '32@ms',
        color: 'white',
        opacity: 0.8
    },
    avatar2: {
        width: '60@ms',
        height: '60@ms',
        borderRadius: '30@ms',
        borderWidth: 1.5,
        borderColor: 'white'
    },
    containerItem: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '25@ms'
    },
    viewRank: {
        alignItems: 'center'
    },
    txtRank: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_SMALL + 8,
        lineHeight: '24@ms',
        color: '#314C1C'
    },
    icoUp: {
        width: '16@ms',
        height: '16@ms'
    },
    footer: {
        height: 130
    },
    viewUsername: {
        backgroundColor: 'white',
        borderRadius: '50@ms',
        flexDirection: 'row',
        alignItems: 'center',
        height: '40@ms',
        marginLeft: '20@ms',
        shadowRadius: 8,
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        elevation: 4,
        flex: 1
    },
    otherAvatar: {
        width: '50@ms',
        height: '50@ms',
        borderRadius: '25@ms',
        marginLeft: '-5@ms'
    },
    txtOtherName: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C',
        marginLeft: '15@ms',
        flex: 1
    },
    txtOtherScore: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE + 2,
        color: '#314C1C',
        justifyContent: 'flex-end',
        marginRight: '10@ms'
    }
});