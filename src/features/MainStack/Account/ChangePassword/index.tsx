import React, { memo, useState } from 'react';
import { View, Alert, Text, TextInput } from 'react-native';
import { font, font_size } from '@/configs/fonts';
import { ScaledSheet } from 'react-native-size-matters';
import { width_screen } from '@/utils';
import { useTranslation } from 'react-i18next';
import { Button } from '@/components/HOC';
import { DynamicHeader } from '@/components/Header';
import { goBack } from '@/utils/navigation';
import { changePassword } from '@/services';
import { useSelector } from 'react-redux';
import { RootState } from '@/redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const ChangePassword = memo(() => {
    const { t } = useTranslation();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const profile: any = useSelector((state: RootState) => state.profileSlice.data);
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword1, setNewPassword1] = useState('');
    const [newPassword2, setNewPassword2] = useState('');
    const [errorState, setErrorState] = useState('');
    const [loadingState, setLoadingState] = useState(false);

    const saveProfile = () => {
        // setLoadingState(true);
        let data = {
            password: oldPassword,
            newPassword: newPassword1,
            confirmPassword: newPassword2
        }

        if (oldPassword == '') {
            setErrorState('Mật khẩu hiện tại không được để trống')
        } else {
            if (newPassword1 == '') {
                setErrorState('Vui lòng nhập mật khẩu mới')
            } else {
                if (newPassword2 == '') {
                    setErrorState('Mật khẩu mới không được để trống')

                } else {
                    if (newPassword1 != newPassword2) {
                        setErrorState('Mật khẩu mới nhập lại phải giống với mật khẩu mới')
                    } else {
                        setLoadingState(true)
                        changePassword(data, token, (res: any) => {
                            console.log("update profile", res)
                            setLoadingState(false);
                            if (res.code == "00") {
                                Alert.alert('Thông báo', 'Bạn đã thay đổi mật khẩu thành công.')
                                goBack();
                            } else {
                                if (res.data?.error) {
                                    setErrorState(res?.data?.error?.password[0])
                                } else {
                                    Alert.alert('Thông báo', 'Có lỗi mạng xảy ra.')
                                }
                            }
                        })
                    }
                }

            }
        }
    }

    return (
        <View style={{ backgroundColor: 'white', flex: 1 }}>
            <DynamicHeader title={'Đổi mật khẩu'} back />
            <KeyboardAwareScrollView enableOnAndroid={false} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <Text style={styles.txtPassword}>Mật khẩu hiện tại</Text>
                    <View style={styles.viewInput}>
                        <TextInput
                            placeholder={'Nhập mật khẩu hiện tại'}
                            value={oldPassword}
                            style={styles.txtInput}
                            onChangeText={setOldPassword}
                            secureTextEntry
                        />
                    </View>
                    <Text style={styles.txtPassword1}>Mật khẩu mới</Text>

                    <View style={styles.viewInput}>
                        <TextInput
                            placeholder={'Nhập mật khẩu mới'}
                            value={newPassword1}
                            style={styles.txtInput}
                            onChangeText={setNewPassword1}
                            secureTextEntry
                        />
                    </View>
                    <View style={styles.viewInput}>
                        <TextInput
                            placeholder={'Nhập lại mật khẩu mới'}
                            value={newPassword2}
                            style={styles.txtInput}
                            onChangeText={setNewPassword2}
                            secureTextEntry
                        />
                    </View>
                    {errorState ?
                        <Text style={styles.txtError}>{errorState}</Text>
                        : <></>
                    }
                </View>
            </KeyboardAwareScrollView>
            <View style={styles.viewBottom}>
                <Button width={width_screen * 0.9}
                    isLoading={loadingState}
                    onPress={saveProfile}>Thay đổi mật khẩu</Button>
            </View>
        </View>
    );
});

const styles = ScaledSheet.create({
    container: {
        backgroundColor: 'white',
        paddingHorizontal: '15@ms',
        flex: 1
    },
    viewBottom: {
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center',
        backgroundColor: 'white',
        width: width_screen,
        alignItems: 'center'
    },
    errorMessage: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_SMALL,
        color: 'red',
        // marginBottom: '5@ms',
        marginLeft: '25@ms',
    },
    txtPassword: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: '#262626',
        marginTop: 10
    },
    txtPassword1: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        color: '#262626',
        marginTop: '15@ms'
    },
    txtInput: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#090A0A',
        //lineHeight: '24@ms',
        paddingVertical: 0,
        flex: 1
    },
    viewInput: {
        marginVertical: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#E3E5E5',
        marginHorizontal: 5
    },
    txtError: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'red',
        marginTop: '5@ms',
        marginLeft: '5@ms'
    }
});

export default ChangePassword;