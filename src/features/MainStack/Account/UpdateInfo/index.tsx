import icons from '@/assets/icons';
import { DatePickerModal, SelectImageModal, SelectModal } from '@/components/Modal';
import { font, font_size } from '@/configs/fonts';
import { RootState } from '@/redux';
import { setProfile } from '@/redux/ProfileSlice';
import { updateProfile } from '@/services';
import { width_screen } from '@/utils';
import { goBack } from '@/utils/navigation';
import moment from 'moment';
import React, { memo, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, Image, Platform, TextInput, Alert } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';
import { useDispatch, useSelector } from 'react-redux';

const UpdateInfo = memo(() => {
    const { t } = useTranslation();
    const token = useSelector((state: RootState) => state.accessTokenSlice.token);
    const profile: any = useSelector((state: RootState) => state.profileSlice.data);
    const [formData, setFormData] = useState<any>(profile);
    const [errorState, setErrorState] = useState<any>({});
    const [name, setName] = useState(formData?.username);
    const genderRef: any = useRef(null);
    const modelSelectImage: any = useRef(null);

    const [dateState, setDateState] = useState(false);
    const [avatar, setAvatar] = useState('');
    const [changedAvatar, setChangedAvatar] = useState(false);

    const isFocusName: any = useRef();

    const [genderSelected, setGenderSelected] = useState<any>({ name: '', id: 0 });
    const [loadingState, setLoadingState] = useState(false);
    const listGender = [
        {
            id: 0,
            name: 'Nữ'
        },
        {
            id: 1,
            name: 'Nam'
        },
        {
            id: 2,
            name: 'Không xác định'
        }
    ];

    const dispatch = useDispatch();

    const updateFormData = (key: string, value: any) => {
        setFormData((preData: any) => {
            return {
                ...preData,
                [key]: value
            }
        });
        setErrorState({});
    };

    useEffect(() => {
        setGenderSelected(listGender.find((item: any) => item.id == formData?.sex))
    }, []);

    useEffect(() => {
        if (profile?.avatar) {
            setAvatar(`http://159.223.76.19:3000/api/user/avatar?img=${profile.avatar}`);
        }
    }, [profile]);

    const bookingInfo = [
        {
            data: formData?.username ? formData.username : 'Chưa có',
            option: 'name',
            type: 'Họ tên',
            onPress: () => isFocusName.current.focus(),
            isPressed: false
        },
        {
            data: profile?.email ? profile.email : 'Chưa có',
            option: 'email',
            type: 'Email',
            onPress: () => { },
            isPressed: false
        },
        {
            data: formData?.dateOfBirth ? formData?.dateOfBirth : 'Chưa có',
            option: 'dob',
            type: 'Ngày sinh',
            onPress: () => setDateState(true),
            isPressed: true
        },
        {
            data: genderSelected ? genderSelected?.name : 'Chưa có',
            option: 'sex',
            type: 'Giới tính',
            onPress: () => genderRef?.current?.open(),
            isPressed: true
        }
    ]

    const onSelectGender = (item: any) => {
        setGenderSelected(item);
        updateFormData('sex', item.id);
    }

    const saveProfile = () => {
        setLoadingState(true);
        let dob = formData?.dateOfBirth instanceof Date ? formData.dateOfBirth : new Date(formData.dateOfBirth)
        let data: any = {
            username: profile?.username,
            date:  dob.toISOString(),
            sex: formData?.sex
        }

        if (changedAvatar) {
            data = {
                ...data,
                avatar: changedAvatar ? formData.avatar : profile.avatar
            }
        }

        if (dob.getTime() > new Date().getTime()) {
            Alert.alert('Thông báo', 'Ngày sinh phải trước ngày hiện tại');
        } else {
            updateProfile(data, token, (res: any) => {
                console.log('update form', res)
                setLoadingState(false);
                if (res.code == "00") {
                    Alert.alert('Thông báo', "Bạn đã cập nhật thông tin thành công");
                    let newData = {
                        ...profile,
                        ...res.data
                    }
                    dispatch(setProfile(newData));
                    goBack()
                } else {
                    Alert.alert('Thông báo', "Không thành công");
                }
            })
        }
    }

    const updateName = (text: any) => {
        setName(text);
        updateFormData('name', text)
    }

    return (
        <>
            <View style={styles.container}>
                <View style={styles.containerTop}>
                    <TouchableOpacity onPress={() => goBack()}>
                        <Image
                            source={icons.header.white_back}
                            style={styles.icoBack}
                        />
                    </TouchableOpacity>
                    <View style={styles.viewInfo}>
                        <View style={styles.containerAvatar}>
                            {avatar ?
                                <FastImage
                                    source={{
                                        uri: avatar,
                                        priority: FastImage.priority.normal,
                                    }}
                                    style={styles.icoAvatar}
                                /> :
                                <Image
                                    source={icons.other.profile}
                                    style={styles.icoAvatar}
                                />
                            }
                            <TouchableOpacity activeOpacity={0.7} onPress={() => modelSelectImage.current.open()}>
                                <Text style={styles.txtChange}>Đổi ảnh</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.containerCnt}>
                        {bookingInfo.map((item, index) => {
                            return (
                                <View style={styles.containerItem} key={index}>
                                    <TouchableOpacity style={styles.viewItem} activeOpacity={0.7}
                                        onPress={item.onPress}>
                                        <Text style={styles.txtType}>{item.type}</Text>
                                        {item.isPressed ?
                                            <View style={styles.viewData}>
                                                <Text style={styles.txtData} numberOfLines={1}>{item.option == 'dob' ? moment(item.data).format('DD/MM/YYYY') : item.data}</Text>
                                                <Image
                                                    source={icons.other.arrowDown}
                                                    style={styles.icon}
                                                />
                                            </View>
                                            :
                                            <View style={[styles.viewInput, { opacity: item.option == 'name' ? 1 : 0.5 }]}>
                                                <TextInput
                                                    ref={item.option == 'name' ? isFocusName : null}
                                                    placeholder={item.data}
                                                    style={styles.txtInput}
                                                    value={item.option == 'name' ? name : profile?.email}
                                                    keyboardType={'default'}
                                                    editable={item.option == 'name' ? true : false}
                                                    onChangeText={(input: any) => item.option == 'name' ? updateName(input) : {}}
                                                />
                                            </View>
                                        }
                                    </TouchableOpacity>
                                </View>
                            )
                        })}
                    </View>
                </View>
            </View>
            <TouchableOpacity activeOpacity={0.7} style={styles.viewEdit} onPress={() => saveProfile()}>
                <Text style={styles.txtEdit}>Chỉnh sửa</Text>
            </TouchableOpacity>
            <SelectModal
                modalRef={genderRef}
                data={{
                    title: 'Giới tính',
                    options: listGender
                }}
                onPress={(item: any) => onSelectGender(item)}
            />
            <DatePickerModal
                isDatePickerVisible={dateState}
                setDatePickerVisibility={(value: boolean) => setDateState(value)}
                value={formData?.dateOfBirth ? new Date(formData?.dateOfBirth) : new Date()}
                setValue={(data: Date) => updateFormData('dateOfBirth', data)}
            />
            <SelectImageModal
                modalRef={modelSelectImage}
                setImageProps={(image: any) => {
                    const filename = new Date().getTime();
                    let fileType = image.path.split('.');
                    fileType = fileType[fileType.length - 1];
                    fileType = fileType.toLowerCase();
                    setAvatar(image.path);
                    var img = {
                        uri: image.path,
                        name: `${filename.toString()}.${fileType}`,
                        type: image.mime,
                    };

                    updateFormData('avatar', img);
                    setChangedAvatar(true);
                }}
            />
        </>
    );
});

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DAE2D5'
    },
    viewInfo: {
        paddingHorizontal: '15@ms',
        paddingTop: Platform.OS == 'android' ? '15@ms' : moderateScale(15),
        alignItems: 'center'
    },
    containerTop: {
        backgroundColor: '#314C1C',
        height: 120,
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40
    },
    containerAvatar: {
        position: 'absolute',
        top: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icoAvatar: {
        width: '100@ms',
        height: '100@ms',
        borderRadius: '55@ms',
        borderWidth: 2,
        borderColor: 'white'
    },
    icoBack: {
        width: '40@ms',
        height: '40@ms',
        marginTop: 10,
        marginLeft: 10
    },
    txtChange: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        backgroundColor: '#314C1C',
        paddingHorizontal: '15@ms',
        paddingVertical: '5@ms',
        borderRadius: '20@ms',
        justifyContent: 'center',
        marginTop: '15@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#314C1C',
        paddingBottom: '8@ms',
        borderBottomWidth: 1
    },
    containerCnt: {
        marginTop: '170@ms',
        marginHorizontal: '15@ms'
    },
    txtName: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#314C1C'
    },
    containerItem: {
        borderBottomWidth: 1,
        borderBottomColor: '#314C1C',
        marginTop: 15
    },
    viewItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width_screen - 40,
        alignItems: 'center',
        height: 50
    },
    viewData: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flex: 3
    },
    viewInput: {
        flex: 3
    },
    txtType: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#090A0A',
        lineHeight: '16@ms',
        flex: 1
    },
    txtData: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#090A0A'
    },
    txtInput: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#090A0A',
        textAlign: 'right',
        paddingVertical: 0,
        flex: 1
    },
    icon: {
        width: '8@ms',
        height: '4@ms',
        marginLeft: '10@ms'
    },
    txtEdit: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: 'white',
        backgroundColor: '#314C1C',
        paddingHorizontal: '25@ms',
        paddingVertical: '10@ms',
        borderRadius: '30@ms',
        justifyContent: 'center',
        marginVertical: '15@ms',
        width: width_screen * 0.4,
        textAlign: 'center'
    },
    viewEdit: {
        backgroundColor: '#DAE2D5',
        alignItems: 'center'
    }
});

export default UpdateInfo;