import React, { memo, useEffect, useRef, useState } from 'react';
import { ScaledSheet } from 'react-native-size-matters';
import { View, Text, TouchableOpacity, Image, TextInput, Alert, Platform, ImageBackground, ScrollView } from 'react-native';
import icons from '@/assets/icons';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { height_screen, width_screen } from '@/utils';
import { Button } from '@/components/HOC';
import { font, font_size } from '@/configs/fonts';
import { register } from '@/services';
import { replace } from '@/utils/navigation';
import { useDispatch } from 'react-redux';
import { setToken } from '@/redux/AccessTokenSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Register = memo(() => {
    const { navigate } = useNavigation();
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const [formUserData, setFormUserData] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
        language: 0
    });

    const [loadingState, setLoadingState] = useState(false);
    const [errorState, setErrorState] = useState<any>();

    const updateFormData = (key: string, value: any) => {
        setFormUserData((preData) => {
            return {
                ...preData,
                [key]: value
            }
        });
    }

    const onRegister = () => {
        setLoadingState(true);
        register(formUserData, async (res: any) => {
            setLoadingState(false);
            if (res.code == "00" && res?.data?.token) {    // Login success
                dispatch(setToken(res.data.token));
                replace('MainStack');
                await AsyncStorage.setItem('access_token', res.data.token);
            } else {
                if (res?.data?.mess){
                    Alert.alert('Thông báo', res?.data?.mess)
                }
            }
        })
    }

    return (
        <ScrollView style={styles.container}>
            {/* <KeyboardAwareScrollView style={styles.center} showsVerticalScrollIndicator={false}> */}
            <ImageBackground style={styles.viewAll} source={icons.introduce.login} resizeMode={'stretch'}>
                <View style={styles.viewContainer}>
                    <Text style={styles.txtLogin}>{t('register.header')}</Text>
                    <View style={styles.viewInput}>
                        <Image source={icons.introduce.username}
                            style={styles.icoUsername} />
                        <TextInput
                            placeholder={'Nhập tên tài khoản'}
                            style={styles.txtInput}
                            onChangeText={(text: any) => updateFormData('username', text)}
                        />
                    </View>
                    <View style={styles.viewInput}>
                        <Image source={icons.introduce.mail}
                            style={styles.icoUsername} />
                        <TextInput
                            placeholder={'Nhập Email'}
                            style={styles.txtInput}
                            onChangeText={(text: any) => updateFormData('email', text)}
                        />
                    </View>
                    <View style={styles.viewInput}>
                        <Image source={icons.introduce.password}
                            style={styles.icoUsername} />
                        <TextInput
                            placeholder={'Nhập mật khẩu'}
                            style={styles.txtInput}
                            onChangeText={(text: any) => updateFormData('password', text)}
                        />
                    </View>
                    <View style={[styles.viewInput, { marginBottom: 20 }]}>
                        <Image source={icons.introduce.password}
                            style={styles.icoUsername} />
                        <TextInput
                            placeholder={'Nhập lại mật khẩu'}
                            style={styles.txtInput}
                            onChangeText={(text: any) => updateFormData('confirmPassword', text)}
                        />
                    </View>
                    <Button
                        customTextStyle={{ color: 'white' }}
                        color={'#3F6766'}
                        width={width_screen * 0.4}
                        onPress={onRegister}
                        isLoading={loadingState}>
                        {t('register.login')}
                    </Button>
                    <View style={styles.viewBottom}>
                        <Text style={styles.txtNoAccount}>{t('register.noAccount')}</Text>
                        <TouchableOpacity onPress={() => navigate('Login')}>
                            <Text style={styles.txtRegister}>{t('register.register')}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.viewRow}>
                        <Image source={{ uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg/2560px-Flag_of_Great_Britain_%281707%E2%80%931800%29.svg.png' }}
                            style={styles.icoFlag}
                        />
                        <Text style={styles.txtLang}>English</Text>
                    </View>
                </View>
            </ImageBackground>
            {/* </KeyboardAwareScrollView> */}
        </ScrollView>
    );
});

const styles = ScaledSheet.create({
    container: {
        flex: 1
    },
    center: {
        marginTop: 0
    },
    viewAll: {
        flex: 1,
        height: height_screen
    },
    viewTop: {
        flex: 1
    },
    viewContainer: {
        backgroundColor: 'white',
        height: height_screen * 0.7,
        marginTop: height_screen * 0.25,
        paddingTop: 30,
        marginHorizontal: 20,
        alignItems: 'center',
        borderRadius: 10,
        shadowRadius: 8,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        elevation: 2
    },
    txtRegister: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: '#3F6766',
        marginVertical: '20@ms'
    },
    txtLogin: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_SMALL * 2,
        color: '#3F6766'
    },
    viewInput: {
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: width_screen * 0.8,
        marginTop: '15@ms'
    },
    icoUsername: {
        width: '24@ms',
        height: '24@ms',
        marginRight: 5
    },
    txtInput: {
        paddingVertical: 0,
        marginVertical: 10,
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        flex: 1
    },
    txtForgot: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: 'rgba(0, 0, 0, 0.5)',
        marginVertical: '10@ms'
    },
    viewBottom: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtNoAccount: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.VERY_LARGE,
        color: 'rgba(0, 0, 0, 0.6)',
        marginRight: 5
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icoFlag: {
        width: 36,
        height: 24,
        marginRight: 10
    },
    txtLang: {
        fontFamily: font.SFProTextMedium,
        fontSize: font_size.VERY_LARGE,
        color: '#3F6766'
    }
});

export default Register;