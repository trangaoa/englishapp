import { DynamicHeader } from "@/components/Header";
import { Button } from "@/components/HOC";
import { font, font_size } from "@/configs/fonts";
import { RootState } from "@/redux";
import { resetPassword } from "@/services";
import { width_screen } from "@/utils";
import { goBack, navigate } from "@/utils/navigation";
import { useRoute } from "@react-navigation/native";
import React, { useState, useRef, useEffect, memo } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Alert
} from "react-native";


const CheckOTP = memo(() => {
    const route: any = useRoute();
    let email = route?.params?.email;
    const [newOtp, setNewOtp] = useState('');
    const [counter, setCounter]: any = useState(120);
    const [errorState, setErrorState] = useState('');

    useEffect(() => {
        if (counter > 0) {
            let interval = setInterval(() => {
                setCounter((lastTimerCount: any) => {
                    lastTimerCount <= 1 && clearInterval(interval)
                    return lastTimerCount - 1
                })
            }, 1000)
            return () => clearInterval(interval)
        } else onBack();
    }, [counter]);

    const onBack = () => {
        setErrorState('Mã OTP của bạn đã hết hạn, vui lòng gửi lại OTP');
    }

    const onSubmit = () => {
        if (newOtp.length == 6) {
            if (newOtp == route?.params?.otp) {
                navigate('ResetPassword', { email: email })
            } else setErrorState('Mã xác thực OTP không đúng')
        } else {
            setErrorState('Mã xác thực OTP là một dãy gồm 6 chữ số')
        }
    }

    const convertTime = () => {
        var minutes: any = Math.floor(counter / 60);
        var seconds: any = counter % 60;

        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return (minutes > 0 ? (minutes + ' phút ') : '') + seconds + ' giây';
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={styles.container}>
                <DynamicHeader back title="Kiểm tra mã OTP" />
                <Text style={styles.txtContent}>Mã xác thực OTP đã được gửi đến email</Text>
                    <Text style={styles.txtEmail}>{email}</Text>
                <View style={{ backgroundColor: 'white', marginVertical: 10 }}>
                    <View style={styles.viewInput}>
                        <TextInput
                            value={newOtp}
                            placeholder={"Mã OTP"}
                            placeholderTextColor={"#c4c4c6"}
                            style={newOtp ? styles.textInput : styles.txtOtp}
                            onChangeText={setNewOtp}
                            autoFocus={true}
                            keyboardType={"numeric"}
                            editable={counter > 0}
                            textAlign="center"
                            textAlignVertical="center"
                            maxLength={6}
                        />
                    </View>
                </View>
                {errorState ?
                    <Text style={{ color: 'red', alignSelf: 'center' }}>{errorState}</Text>
                    : <></>
                }
                <View style={styles.linearGradient}>
                    <Button width={width_screen * 0.4}
                        disabled={counter == 0 || newOtp == ''}
                        onPress={onSubmit}>
                        Xác thực
                    </Button>
                </View>
                <Text style={styles.txtCount1}>Mã OTP của bạn sẽ hết hạn sau </Text>
                <Text style={styles.txtCount}>{convertTime()}</Text>
            </View>

        </View>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textInput: {
        fontFamily: font.SFProTextMedium,
        fontSize: font_size.VERY_LARGE * 1.5,
        padding: 0,
        letterSpacing: 25,
        flex: 1
    },
    txtOtp: {
        fontFamily: font.SFProTextMedium,
        fontSize: font_size.VERY_LARGE * 1.5,
        marginTop: 10,
        padding: 0,
        flex: 1
    },
    viewInput: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: 'center',
        marginVertical: 20,
        marginHorizontal: 15,
        borderWidth: 1,
        justifyContent: 'center',
        borderColor: "#8C8C8C",
        width: width_screen * 0.65,
        borderRadius: 10,
        height: 60
    },
    buttonBottom: {
        height: 48,
        borderRadius: 8,
        backgroundColor: '#237DF3',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        width: "95%",
        alignSelf: 'center'
    },
    txtButtonText: {
        fontFamily: font.SFProTextSemibold,
        fontSize: 16,
        color: "#FFFFFF",
    },
    linearGradient: {
        marginTop: 10,
        borderRadius: 4,
        marginHorizontal: 10,
        alignSelf: 'center'
    },
    btnLogin: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        borderRadius: 4,
    },
    txtCount1: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        alignSelf: 'center',
        color: 'black',
        marginTop: 20
    },
    txtContent: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        alignSelf: 'center',
        color: 'black',
        marginTop: 20,
        marginHorizontal: 30,
        textAlign: 'center'
    },
    txtCount: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.VERY_LARGE,
        alignSelf: 'center',
        color: 'rgba(49, 76, 28, 0.8)'
    },
    txtEmail: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.NORMAL,
        alignSelf: 'center',
        color: 'black'
    },
});

export default CheckOTP;