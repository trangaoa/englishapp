import { DynamicHeader } from "@/components/Header";
import { Button } from "@/components/HOC";
import { font, font_size } from "@/configs/fonts";
import { RootState } from "@/redux";
import { resetPassword } from "@/services";
import { width_screen } from "@/utils";
import { goBack, navigate } from "@/utils/navigation";
import { useRoute } from "@react-navigation/native";
import React, { useState, useRef, useEffect, memo } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Alert, Platform
} from "react-native";

const ChangeForgotPassword = memo(() => {
    const route: any = useRoute();
    let email = route?.params?.email;
    const [newPassword1, setNewPassword1] = useState('');
    const [newPassword2, setNewPassword2] = useState('');
    const [errorState, setErrorState] = useState('');
    const [isLoading, setLoading] = useState(false);

    const saveProfile = () => {
        let data = {
            email: email,
            newPassword: newPassword1,
            confirmPassword: newPassword2
        }

        if (newPassword1 == '') {
            setErrorState('Vui lòng nhập mật khẩu mới')
        } else {
            if (newPassword2 == '') {
                setErrorState('Xác nhận mật khẩu mới không được để trống')
            } else {
                if (newPassword1 != newPassword2) {
                    setErrorState('Mật khẩu mới phải giống với mật khẩu xác nhận')
                } else {
                    setLoading(true)
                    resetPassword(data, (res: any) => {
                        console.log("update profile", res)
                        setLoading(false);
                        if (res.code == "00") {
                            Alert.alert('Thông báo', 'Bạn đã thay đổi mật khẩu thành công.')
                            navigate('Login')
                        } else {
                            Alert.alert('Thông báo', 'Có lỗi xảy ra.')
                        }
                    })
                }
            }
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.container}>
                <DynamicHeader back title="Thay đổi mật khẩu" />
                <View style={{ backgroundColor: 'white', marginVertical: 10 }}>
                    <View style={styles.viewInput}>
                        <TextInput
                            value={newPassword1}
                            placeholder={"Nhập mật khẩu mới"}
                            placeholderTextColor={"#c4c4c6"}
                            style={styles.textInput}
                            onChangeText={setNewPassword1}
                            autoFocus={true}
                            secureTextEntry
                        />
                    </View>
                    <View style={styles.viewInput}>
                        <TextInput
                            value={newPassword2}
                            placeholder={"Nhập lại mật khẩu mới"}
                            placeholderTextColor={"#c4c4c6"}
                            style={styles.textInput}
                            onChangeText={setNewPassword2}
                            secureTextEntry
                        />
                    </View>
                </View>
                {errorState ?
                    <Text style={{ color: 'red' }}>{errorState}</Text>
                    : <></>
                }
                <View style={styles.linearGradient}>
                    <Button width={width_screen * 0.6}
                        isLoading={isLoading}
                        onPress={saveProfile}>
                        Lưu mật khẩu
                    </Button>
                </View>
            </View>

        </View>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    textInput: {
        fontFamily: font.SFProTextMedium,
        fontSize: font_size.NORMAL,
        padding: 10,
        borderBottomColor: "#8C8C8C",
        borderBottomWidth: 1,
        flex: 1
    },
    viewInput: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 15,
        marginHorizontal: 15,
    },
    buttonBottom: {
        height: 48,
        borderRadius: 8,
        backgroundColor: '#237DF3',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        width: "95%",
        alignSelf: 'center'
    },
    txtButtonText: {
        fontFamily: font.SFProTextSemibold,
        fontSize: 16,
        color: "#FFFFFF",
    },
    linearGradient: {
        marginTop: 20,
        borderRadius: 4,
        marginHorizontal: 10,
        alignSelf: 'center'
    },
    btnLogin: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        borderRadius: 4,
    },
});

export default ChangeForgotPassword;
