import React, { Component, useState, useEffect, memo } from 'react';
import {
    StatusBar,
    View,
    Text,
    ActivityIndicator,
    TextInput,
    Alert
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { DynamicHeader } from '@/components/Header';
import { Button } from '@/components/HOC';
import { ScaledSheet } from 'react-native-size-matters';
import { width_screen } from '@/utils';
import { font, font_size } from '@/configs/fonts';
import { forgotPassword } from '@/services';
import { navigate } from '@/utils/navigation';

const NewPassword = memo(() => {
    const [email, setEmail] = useState("");
    const [isLoading, setLoading] = useState(false);
    const [errorState, setErrorState] = useState('');

    const onSendOTP = () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(email)) {
            let data = {
                email: email
            }
            setLoading(true)
            forgotPassword(data, (res: any) => {
                setLoading(false)
                if (res.code == "00") {
                    console.log('res', res)
                    navigate('CheckOTP', { otp: res?.data, email: email })
                } else {
                    setErrorState('Email không hợp lệ')
                }
            })
        } else {
            setErrorState('Vui lòng nhập đúng định dạng email')
        }
    }


    return (
        <>
            <StatusBar barStyle="light-content" />
            <DynamicHeader title="Quên mật khẩu" back />
            <View style={styles.container}>
                <KeyboardAwareScrollView
                    style={styles.form}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.viewInput}>
                        <TextInput
                            value={email}
                            placeholder={"Nhập Email của bạn"}
                            placeholderTextColor={"#c4c4c6"}
                            style={styles.textInput}
                            onChangeText={(text) => setEmail(text)}
                            autoFocus={true}
                            keyboardType={"email-address"}
                            returnKeyType="done"
                        />
                    </View>
                    {errorState ?
                        <Text style={{ color: 'red', alignSelf: 'center' }}>{errorState}</Text>
                        : <></>
                    }
                    <View style={styles.linearGradient}>
                        <Button width={width_screen * 0.5}
                            isLoading={isLoading}
                            disabled={email ? false : true}
                            onPress={onSendOTP}>
                            Gửi OTP
                        </Button>
                    </View>
                    <Text style={[styles.txtDes, { marginTop: 20 }]}>Chúng tôi sẽ gửi một mã xác thực đến</Text>
                    <Text style={styles.txtDes}>email của bạn để giúp bạn tạo lại mật khẩu mới</Text>
                </KeyboardAwareScrollView>
            </View>
        </>
    );
});

export default NewPassword;

const styles = ScaledSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    form: {
        margin: '10@ms'
    },
    viewInput: {
        paddingVertical: 15,
        marginHorizontal: 10
    },
    txtTitle: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
    },
    txtTitle1: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        marginTop: '10@ms'
    },
    textInput: {
        fontFamily: font.SFProTextMedium,
        color: 'black',
        fontSize: font_size.NORMAL,
        padding: 10,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    txtInput: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
    },
    viewAlert: {
        backgroundColor: '#21a659',
        padding: 15,
        marginTop: 20,
        borderRadius: 5,
    },
    linearGradient: {
        marginTop: 20,
        borderRadius: 4,
        alignSelf: 'center'
    },
    btnLogin: {
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        borderRadius: 4,
    },
    txtAlert: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
    },
    errorMessage: {
        marginTop: 10,
        textAlign: 'center',
        color: 'red'
    },
    successMessage: {
        marginTop: 10,
        textAlign: 'center',
        color: 'green'
    },
    txtDes: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#595959',
        marginTop: 5,
        textAlign: 'center'
    }
});
