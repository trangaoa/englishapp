import React, { memo, useState } from 'react';
import { ScaledSheet } from 'react-native-size-matters';
import { View, Text, TouchableOpacity, Image, TextInput, ImageBackground, ScrollView } from 'react-native';
import icons from '@/assets/icons';
import { useNavigation } from '@react-navigation/native';
import { useTranslation } from 'react-i18next';
import { height_screen, width_screen } from '@/utils';
import { Button } from '@/components/HOC';
import { font, font_size } from '@/configs/fonts';
import { login } from '@/services';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { setToken } from '@/redux/AccessTokenSlice';
import { replace } from '@/utils/navigation';

const Login = memo(() => {
  const { navigate } = useNavigation();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    username: '',
    password: '',
  });

  const [loadingState, setLoadingState] = useState(false);
  const [secure, setSecure] = useState(true);
  const [errorState, setErrorState] = useState<any>();

  const updateFormData = (key: string, value: any) => {
    setFormData((preData) => {
      return {
        ...preData,
        [key]: value
      }
    });
  }

  const onLogin = () => {
    if (formData.username == '') {
      setErrorState('Vui lòng nhập tên tài khoản')
    } else {
      if (formData.password == '') {
        setErrorState('Vui lòng nhập mật khẩu')
      } else {
        setLoadingState(true);
        login(formData, async (res: any) => {
          console.log('login', res)
          setLoadingState(false);
          if (res.code == "00" && res.data?.token) {    // Login success
            dispatch(setToken(res.data.token));
            replace('MainStack');
            await AsyncStorage.setItem('access_token', res.data.token);
          } else {
            if (res.code == "09") {
              setErrorState('Tài khoản chưa tồn tại. Bạn vui lòng đăng ký');
            }
            if (res?.data?.mess){
              setErrorState(res?.data?.mess)
            }
          }
        })
      }
    }
  }

  return (
    <ScrollView style={styles.container}>
      <ImageBackground style={styles.viewAll} source={icons.introduce.login} resizeMode={'stretch'}>
        <View style={styles.viewContainer}>
          <Text style={styles.txtLogin}>{t('login.header')}</Text>
          <View style={styles.viewInput}>
            <Image source={icons.introduce.username} style={styles.icoUsername} />
            <TextInput
              placeholder={'Nhập tên tài khoản'}
              style={styles.txtInput}
              onChangeText={(text: any) => updateFormData('username', text)}
            />
          </View>
          <View style={styles.viewInput}>
            <View style={styles.viewRow}>
              <Image source={icons.introduce.password} style={styles.icoUsername} />
              <TextInput
                placeholder={'Nhập mật khẩu'}
                style={styles.txtInput}
                onChangeText={(text: any) => updateFormData('password', text)}
                secureTextEntry={secure}
              />
            </View>
            <TouchableOpacity onPress={() => setSecure(!secure)}>
              {secure ?
                <Image source={icons.introduce.secure} style={styles.icoEye} />
                : <Image source={icons.introduce.eye} style={styles.icoEye} />
              }
            </TouchableOpacity>
          </View>
          <Text style={styles.txtError}>{errorState ? errorState : ''}</Text>
          <TouchableOpacity activeOpacity={0.7} onPress={() => navigate('ForgotPassword')}>
            <Text style={styles.txtForgot}>{t('login.forgot')}</Text>
          </TouchableOpacity>
          <Button
            customTextStyle={{ color: 'white' }}
            color={'#3F6766'}
            width={width_screen * 0.4}
            onPress={onLogin}
            isLoading={loadingState}>
            {t('login.login')}
          </Button>
          <View style={styles.viewBottom}>
            <Text style={styles.txtNoAccount}>{t('login.noAccount')}</Text>
            <TouchableOpacity onPress={() => navigate('Register')}>
              <Text style={styles.txtRegister}>{t('login.register')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </ScrollView>
  );
});

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  viewAll: {
    flex: 1,
    height: height_screen
  },
  viewContainer: {
    backgroundColor: 'white',
    height: height_screen * 0.6,
    marginTop: height_screen * 0.3,
    paddingTop: 30,
    marginHorizontal: 20,
    alignItems: 'center',
    borderRadius: 10,
    shadowRadius: 8,
    shadowColor: 'black',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    elevation: 2
  },
  txtRegister: {
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    color: '#3F6766',
    marginVertical: '30@ms'
  },
  txtLogin: {
    fontFamily: font.SFProTextSemibold,
    fontSize: font_size.VERY_SMALL * 2,
    color: '#3F6766'
  },
  viewInput: {
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    width: width_screen * 0.8,
    marginTop: '15@ms'
  },
  icoUsername: {
    width: '24@ms',
    height: '24@ms',
    marginRight: 5
  },
  icoEye: {
    width: '20@ms',
    height: '20@ms',
    tintColor: 'rgba(0, 0, 0, 0.5)',
  },
  txtInput: {
    paddingVertical: 0,
    marginVertical: 10,
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    flex: 1
  },
  txtForgot: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    color: 'rgba(0, 0, 0, 0.5)',
    marginBottom: '10@ms'
  },
  viewBottom: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  txtNoAccount: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_LARGE,
    color: 'rgba(0, 0, 0, 0.6)',
    marginRight: 5
  },
  txtError: {
    fontFamily: font.SFProTextRegular,
    fontSize: font_size.VERY_SMALL,
    color: 'red',
    paddingTop: '10@ms'
  },
  viewRow: {
    alignItems: 'center',
    flexDirection: 'row',
    width: '90%'
  }
});

export default Login;