import React, { memo, useState } from 'react';
import { TouchableOpacity, Text, FlatList, View, Image, RefreshControl, ActivityIndicator } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
// @ts-ignore
import { font, font_size } from '@/configs/fonts';
import icons from '@/assets/icons';
import { EmptyState } from '.';

interface Props {
    data: Array<any>,
    onPress?: any,
    onPressAnswer?: any,
    onRefresh?: any,
    onLoadMore?: any,
    onChooseMulti?: any,
    isMultiple?: boolean,
    isLoadingMore?: boolean,
    setData?: any,
    isPressAnswer?: boolean
};

export const FlatListQuestion = memo(function FlatListCart({
    data, onPress, onPressAnswer, onRefresh, onChooseMulti, onLoadMore, isLoadingMore, setData, isPressAnswer
}: Props) {
    const [isEnd, setEnd] = useState(false);

    const onChooseMany = (item: any) => {
        let newValue = [...data];
        let found = newValue.findIndex((obj: any) => obj.id == item.id);
        if (found != -1) {
            let tmp = newValue[found]?.isChoosed;
            newValue[found].isChoosed = !tmp;
        }
        setData(newValue);
        onChooseMulti(newValue)
    }

    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                style={{ marginBottom: 0 }}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (<RenderItem item={item} index={index} onPress={onChooseMulti ? onChooseMany : onPress ? onPress : null} onPressAnswer={onPressAnswer ? onPressAnswer : null} isPressed={isPressAnswer} />)}
                refreshControl={onRefresh ? <RefreshControl refreshing={false} onRefresh={onRefresh} tintColor={'gray'} /> : <></>}
                onEndReachedThreshold={0.5}
                onEndReached={(distance: any) => setEnd(distance < 0 ? false : true)}
                onMomentumScrollBegin={isEnd ? onLoadMore : () => { }}
                ListFooterComponent={
                    isLoadingMore ?
                        <ActivityIndicator size={'small'} color={'gray'} />
                        : <></>
                }
                ListEmptyComponent={<EmptyState content={'Chưa có dữ liệu'} />}
            />
        </View>
    );
});

const RenderItem = memo(({ item, index, onPress, onPressAnswer, isPressed }: { item: any, index: any, onPress?: any, onPressAnswer?: any, isPressed?: boolean }) => {
    const [isChoosed, setChoosed]: any = useState();
    const [isSelected, setSelected]: any = useState();
    const [isLiked, setLiked]: any = useState();

    const onChooseAnswer = (value: any) => {
        onPressAnswer ? onPressAnswer(item, value) : null;
        setChoosed(value)
    }

    const onSelectAnswer = (value: any) => {
        onPressAnswer(item, value)
        setSelected(value)
    }

    return (
        <TouchableOpacity style={styles.containerItem} disabled={onPress ? false : true} activeOpacity={0.7} onPress={() => onPress(item)}>
            <View style={[styles.viewItem, { opacity: item?.isChoosed ? 0.6 : 1 }]}>
                <View style={styles.viewNumber}>
                    <View style={styles.viewRow}>
                        <Text style={styles.txtNumber} numberOfLines={1}>{`Câu hỏi ${index + 1}:`}</Text>
                        <Text style={[styles.txtCorrect, { color: item.level == 1 ? '#54A019' : item.level == 2 ? '#CC8A26' : '#BC2525' }]} numberOfLines={1}>{item.level == 1 ? 'Dễ' : (item.level == 2 ? 'Trung bình' : 'Khó')}</Text>
                    </View>
                    <View style={styles.viewRow}>
                        <TouchableOpacity activeOpacity={0.5} onPress={() => setLiked(!isLiked)}>
                            {isLiked ?
                                <Image
                                    source={icons.question.liked}
                                    style={styles.icoHeart}
                                />
                                : <Image
                                    source={icons.question.heart}
                                    style={styles.icoHeart}
                                />
                            }
                        </TouchableOpacity>
                        <Image
                            source={icons.question.menu}
                            style={styles.icoHeart}
                        />
                    </View>
                </View>
                <Text style={styles.txtQuestion}>{item?.title}</Text>
            </View>
            {item?.answers && item.answers.length > 0 ?
                <FlatList
                    data={item.answers}
                    showsVerticalScrollIndicator={false}
                    scrollEnabled={false}
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (<RenderQuestion item={item} index={index} disabled={onPress ? true : false} isChoosed={isChoosed} isSelected={isSelected} onPress={(obj: any) => isPressed ? onSelectAnswer(obj) : onChooseAnswer(obj)} />)}
                    style={{ marginLeft: 20 }}
                />
                : <></>
            }
            {isChoosed ?
                <View style={styles.viewExplain}>
                    <Text style={styles.txtExplain}>Giải thích</Text>
                    <Text style={styles.txtExplain1}>{isChoosed.description ? isChoosed.description : 'Chưa có giải thích cho đáp án này'}</Text>
                </View>
                : <></>
            }
        </TouchableOpacity>
    )
});

const RenderQuestion = memo(({ item, index, isChoosed, isSelected, onPress, disabled }: any) => {
    const onSelect = () => {
        onPress(item)
    }

    return (
        isSelected && isSelected?.id == item.id ?
            <TouchableOpacity style={{ flex: 1 }} activeOpacity={0.7} onPress={() => onSelect()} disabled={isChoosed || disabled}>
                <Text style={[styles.txtAnswer, { backgroundColor: '#C0D6B2' }]}>{index == 0 ? 'A' : (index == 1 ? 'B' : index == 2 ? 'C' : 'D')}. {item.content}</Text>
            </TouchableOpacity>
            : <TouchableOpacity style={{ flex: 1 }} activeOpacity={0.7} onPress={() => onSelect()} disabled={isChoosed || disabled}>
                <Text style={[isChoosed && isChoosed.id == item.id ? [styles.txtCorrectAnswer, { backgroundColor: isChoosed?.isTrue ? '#54A019' : '#BC2525' }] : styles.txtAnswer]}>{index == 0 ? 'A' : (index == 1 ? 'B' : index == 2 ? 'C' : 'D')}. {item.content}</Text>
            </TouchableOpacity>
    )
});

const styles = ScaledSheet.create({
    container: {
        flex: 1
    },
    containerItem: {
        marginBottom: '10@ms',
        backgroundColor: 'white',
        padding: '15@ms'
    },
    viewItem: {
        borderRadius: '10@ms',
        borderWidth: 0.5,
        borderColor: '#314C1C'
    },
    viewNumber: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    txtNumber: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        backgroundColor: '#314C1C',
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingVertical: 5,
        paddingHorizontal: '20@ms'
    },
    txtCorrect: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#54A019',
        marginVertical: '5@ms',
        marginLeft: '10@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icoHeart: {
        width: '20@ms',
        height: '20@ms',
        marginRight: '5@ms'
    },
    txtQuestion: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        marginVertical: '5@ms',
        marginHorizontal: '10@ms'
    },
    txtAnswer: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        paddingVertical: '6@ms',
        paddingHorizontal: '10@ms',
        borderWidth: 1,
        borderColor: '#314C1C',
        borderRadius: '20@ms',
        flex: 1,
        textAlign: 'center',
        marginTop: '10@ms',
        marginRight: '10@ms'
    },
    txtCorrectAnswer: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        paddingVertical: '6@ms',
        paddingHorizontal: '15@ms',
        backgroundColor: '#54A019',
        borderRadius: '20@ms',
        flex: 1,
        textAlign: 'center',
        marginTop: '10@ms',
        marginRight: '10@ms'
    },
    viewExplain: {
        backgroundColor: 'rgba(218, 226, 213, 0.8)',
        paddingHorizontal: '10@ms',
        paddingVertical: '6@ms',
        borderRadius: '10@ms',
        marginTop: '15@ms',
        borderWidth: 0.25,
        borderColor: '#314C1C'
    },
    txtExplain: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.NORMAL,
        color: '#314C1C',

    },
    txtExplain1: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C'
    }
})