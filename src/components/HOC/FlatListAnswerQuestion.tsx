import React, { memo } from 'react';
import { TouchableOpacity, Text, FlatList, View, Image } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
// @ts-ignore
import { font, font_size } from '@/configs/fonts';
import icons from '@/assets/icons';

interface Props {
    data: Array<any>
};

export const FlatListAnswerQuestion = memo(function FlatListCart({
    data
}: Props) {
    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (<RenderItem item={item} index={index} />)}
            />
        </View>
    );
});

const RenderItem = memo(({ item, index }: { item: any, index: any }) => {
    let answerId = item?.answerId ? item?.answerId : null;
    let found = item.answers.findIndex((obj: any) => obj.id == answerId);
    let check = item.check;

    return (
        <View style={styles.containerItem}>
            <View style={styles.viewItem}>
                <View style={styles.viewNumber}>
                    <View style={styles.viewRow}>
                        <Text style={styles.txtNumber} numberOfLines={1}>{`Câu hỏi ${index + 1}:`}</Text>
                        <Text style={[styles.txtCorrect, { color: item.level == 1 ? '#54A019' : item.level == 2 ? '#CC8A26' : '#BC2525' }]} numberOfLines={1}>{item.level == 1 ? 'Dễ' : (item.level == 2 ? 'Trung bình' : 'Khó')}</Text>
                    </View>
                    <View style={styles.viewRow}>
                        <Image
                            source={icons.question.heart}
                            style={styles.icoHeart}
                        />
                        <Image
                            source={icons.question.menu}
                            style={styles.icoHeart}
                        />
                    </View>
                </View>
                <Text style={styles.txtQuestion}>{item.title}</Text>
            </View>
            <FlatList
                data={item.answers}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (<RenderQuestion item={item} index={index} check={check} isChoosed={answerId} />)}
                style={{ marginLeft: 20 }}
            />
                <View style={styles.viewExplain}>
                    <Text style={styles.txtExplain}>Giải thích</Text>
                    <Text style={styles.txtExplain1}>{found != -1 ? item.answers[found]?.description ? item.answers[found]?.description : 'Chưa có lời giải thích cho đáp án này' : 'Chưa chọn đáp án'}</Text>
                </View>
        </View>
    )
});

const RenderQuestion = memo(({ item, index, check, isChoosed }: any) => {
    return (
        isChoosed && item.id == isChoosed ?
            <Text style={[styles.txtCorrectAnswer, { backgroundColor: check ? '#54A019' : '#BC2525' }]}>{index == 0 ? 'A' : (index == 1 ? 'B' : (index == 2 ? 'C' : 'D'))}. {item.content}</Text>
            : <Text style={styles.txtAnswer}>{index == 0 ? 'A' : (index == 1 ? 'B' : (index == 2 ? 'C' : 'D'))}. {item.content}</Text>
    )
});

const styles = ScaledSheet.create({
    container: {
        // marginBottom: '20@ms'
    },
    containerItem: {
        marginBottom: '10@ms',
        backgroundColor: 'white',
        padding: '15@ms'
    },
    viewItem: {
        borderRadius: '10@ms',
        borderWidth: 0.5,
        borderColor: '#314C1C'
    },
    viewNumber: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    txtNumber: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        backgroundColor: '#314C1C',
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingVertical: 5,
        paddingHorizontal: '20@ms'
    },
    txtCorrect: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#54A019',
        marginVertical: '5@ms',
        marginLeft: '10@ms'
    },
    viewRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icoHeart: {
        width: '20@ms',
        height: '20@ms',
        marginRight: '5@ms'
    },
    txtQuestion: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        marginVertical: '5@ms',
        marginHorizontal: '10@ms'
    },
    txtAnswer: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C',
        paddingVertical: '6@ms',
        paddingHorizontal: '15@ms',
        borderWidth: 1,
        borderColor: '#314C1C',
        borderRadius: '20@ms',
        flex: 1,
        marginRight: '20@ms',
        textAlign: 'center',
        marginTop: '10@ms'
    },
    txtCorrectAnswer: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: 'white',
        paddingVertical: '6@ms',
        marginRight: '20@ms',
        paddingHorizontal: '15@ms',
        backgroundColor: '#54A019',
        borderRadius: '20@ms',
        flex: 1,
        textAlign: 'center',
        marginTop: '10@ms'
    },
    viewExplain: {
        backgroundColor: 'rgba(218, 226, 213, 0.8)',
        paddingHorizontal: '10@ms',
        paddingVertical: '6@ms',
        borderRadius: '10@ms',
        marginTop: '15@ms',
        borderWidth: 0.25,
        borderColor: '#314C1C'
    },
    txtExplain: {
        fontFamily: font.SFProTextSemibold,
        fontSize: font_size.NORMAL,
        color: '#314C1C',

    },
    txtExplain1: {
        fontFamily: font.SFProTextRegular,
        fontSize: font_size.NORMAL,
        color: '#314C1C'
    }
})