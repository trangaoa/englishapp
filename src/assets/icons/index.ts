const icons = {
    header: {
        back: require('./Header/back.png'),
        search: require('./Home/search.png'),
        menu: require('./Home/menu.png'),
        filter: require('./Header/filter.png'),
        white_back: require('./Header/white-back.png'),
        arrow_next: require('./Header/arrow-next.png'),
        arrow_prev: require('./Header/arrow-prev.png'),
        speaker: require('./Header/speaker.png'),
    },
    introduce: {
        background: require('./Introduce/background.png'),
        login: require('./Introduce/login.png'),
        username: require('./Introduce/username.png'),
        password: require('./Introduce/password.png'),
        mail: require('./Introduce/mail.png'),
        eye: require('./Introduce/eye.png'),
        secure: require('./Introduce/secure.png'),
    },
    tabbar: {
        home: require('./Tabbar/home.png'),
        booking: require('./Tabbar/booking.png'),
        messages: require('./Tabbar/messages.png'),
        account: require('./Tabbar/account.png')
    },
    other: {
        back: require('./Other/ic_back.png'),
        arrowDown: require('./Other/ic_down.png'),
        arrowRight: require('./Other/ic_right.png'),
        close: require('./Other/close.png'),
        camera: require('./Other/camera.png'),
        image: require('./Other/image.png'),
        profile: require('./Other/profile.png')
    },
    homepage: {
        add: require('./Home/add.png'),
        shuffle: require('./Home/shuffle.png'),
    },
    account: {
        coin: require('./Account/coin.png'),
        next: require('./Account/next.png'),
        edit: require('./Account/edit.png'),
        package: require('./Account/package.png'),
        question: require('./Account/question.png'),
        up: require('./Account/up.png'),
        tag: require('./Account/tag.png'),
    },
    profile: {
        tag: require('./Profile/tag.png'),
        star: require('./Profile/star.png'),
        next: require('./Profile/next.png')
    },
    package: {
        star: require('./Package/star.png'),
        like: require('./Package/like.png'),
        dislike: require('./Package/dislike.png'),
        comment: require('./Package/comment.png'),
        share: require('./Package/share.png'),
        add: require('./Package/add.png'),
        search: require('./Package/search.png'),
    },
    question: {
        heart: require('./Question/heart.png'),
        menu: require('./Question/menu.png'),
        liked: require('./Question/liked.png')
    },
    rank: {
        crown: require('./Rank/crown.png')
    },
    side_menu: {
        account: require('./SideMenu/account.png'),
        info: require('./SideMenu/info.png'),
        log_out: require('./SideMenu/log-out.png'),
        password: require('./SideMenu/password.png'),
        setting: require('./SideMenu/setting.png'),
        language: require('./SideMenu/language.png'),
        dark: require('./SideMenu/dark.png'),
    },
    sound: {
        play: require('./Sound/play.png'),
        pause: require('./Sound/pause.png'),
        prev: require('./Sound/prev.png'),
        next: require('./Sound/next.png'),
        reset: require('./Sound/reset.png'),
        speaker: require('./Sound/speaker.png')
    }
}

export default icons;