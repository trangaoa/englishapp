import { convertObToUrl } from '@/utils';
import { API, APIFormData } from './request';

export const register = (data: object, callback: any) => {
    API('auth/signup', 'post', null, data, (res: object) => callback(res));
};

export const login = (data: object, callback: any) => {
    API('auth/login', 'post', null, data, (res: object) => callback(res));
};

export const forgotPassword = (data: any, callback: any) => {
    API('auth/forgot-password', 'post', null, data, (res: object) => callback(res));
};

export const resetPassword = (data: object, callback: any) => {
    API('auth/reset-password', 'post', null, data, (res: object) => callback(res));
};

export const changePassword = (data: object, token: any, callback: any) => {
    API('auth/change-password', 'post', token, data, (res: object) => callback(res));
};

export const getProfile = ( token: any, callback: any) => {
    API('user/profile', 'get', token, null, (res: object) => callback(res));
};

export const getAnotherProfile = (userId: any, token: any, callback: any) => {
    API(`user/profile?user_id=${userId}`, 'post', token, null, (res: object) => callback(res));
};

// export const updateProfile = (data: object, token: any, callback: any) => {
//     API('user/update-profile', 'post', token, data, (res: object) => callback(res));
// };

export const updateProfile = (data: any, token: any, callback: any) => {
    const bodyFormData = new FormData();
    for (let key in data) {
        if (data[key]) {
            if (typeof data[key] === 'object') {
                bodyFormData.append(key, data[key]);
            } else {
                bodyFormData.append(key, data[key].toString());
            }
        }
    }
    APIFormData('user/update-profile', 'post', token, bodyFormData, (res: object) => callback(res));
};

export const getRandomQuestion = (token: any, data: any, callback: any) => {
    API('question/list-random-question', 'post', token, data, (res: object) => callback(res));
};

export const getListQuestion = (data: any, token: any, callback: any) => {
    API('question/list-question', 'post', token, data, (res: object) => callback(res));
};

export const doQuestion = (data: any, token: any, callback: any) => {
    API('question/do-question', 'post', token, data, (res: object) => callback(res));
};

export const createQuestion = (data: any, token: any, callback: any) => {
    API('question/create-question', 'post', token, data, (res: object) => callback(res));
};

export const getAllPackage = (data: any, token: any, callback: any) => {
    API('package/all-package', 'post', token, data, (res: object) => callback(res));
};

export const getPackageDetails = (data: any, token: any, callback: any) => {
    API(`package/get-detail-package`, 'post', token, data, (res: object) => callback(res));
};

export const createPackage = (data: any, token: any, callback: any) => {
    API('package/create-package', 'post', token, data, (res: object) => callback(res));
};

export const doPackage = (data: object, token: any, callback: any) => {
    API('package/do-package', 'post', token, data, (res: object) => callback(res));
};

// Lịch sử làm bài
export const getHistory = (data: object, token: any, callback: any) => {
    API('package/get-history', 'post', token, data, (res: object) => callback(res));
};

// Tất cả lịch sử làm bài
export const getHistoryPackage = (data: any, token: any, callback: any) => {
    API(`package/get-detail-package-history`, 'post', token, data, (res: object) => callback(res));
};

// Chi tiết lịch sử lần làm bài
export const getHistoryDetail = (data: any, token: any, callback: any) => {
    API(`package/get-detail-history`, 'post', token, data, (res: object) => callback(res));
};

export const getLeaderboard = (data: any, token: any, callback: any) => {
    API('package/leaderboard', 'post', token, data, (res: object) => callback(res));
};

// Thống kê câu hỏi, bộ câu hỏi
export const getStatics = (token: any, callback: any) => {
    API('question/get-statics', 'get', token, null, (res: object) => callback(res));
};

export const approveQuestion = (data: any, token: any, callback: any) => {
    API('admin/approve', 'post', token, data, (res: object) => callback(res));
};

// Lấy danh sách câu chuyện
export const getStories = (data: any, token: any, callback: any) => {
    API('user/list-story', 'post', token, data, (res: object) => callback(res));
};

export const getStoryDetail = (data: any, token: any, callback: any) => {
    API('user/get-detail-story', 'post', token, data, (res: object) => callback(res));
};

// Gợi ý từ khóa
export const suggestKeyword = (keyword: any, token: any, callback: any) => {
    API('question/suggest', 'post', token, {search: keyword}, (res: object) => callback(res));
};

// Thông tin từ khóa
export const findKeyword = (keyword: any, token: any, callback: any) => {
    API('question/find', 'post', token, {search: keyword}, (res: object) => callback(res));
};
