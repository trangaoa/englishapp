import { combineReducers } from 'redux';
import AccessTokenSlice from './AccessTokenSlice';
import ProfileSlice from './ProfileSlice';
import PackagesSlice from './PackagesSlice';

const rootReducer = combineReducers({
    accessTokenSlice: AccessTokenSlice,
    profileSlice: ProfileSlice,
    packagesSlice: PackagesSlice
});
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
