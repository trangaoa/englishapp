import { createSlice } from '@reduxjs/toolkit';
import { packagesSliceProps } from './types';

const initialState = {
    data: ''
} as packagesSliceProps;

const packagesSlice = createSlice({
    name: 'packages',
    initialState,
    reducers: {
        setPackages(state, action) {
            state.data = action.payload;
        },
    }
});

export const { setPackages } = packagesSlice.actions;
export default packagesSlice.reducer;