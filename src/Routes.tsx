import React, { memo, useRef, useCallback } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from '@/utils/navigation';
const RootStack = createStackNavigator();
const AuthStack = createStackNavigator();
const MainStack = createStackNavigator();

const AuthStackComponent = memo(function AuthStackComponent() {
  return (
    <AuthStack.Navigator initialRouteName={'Introduce'} headerMode={'none'}>
      <AuthStack.Screen name={'Introduce'} component={require('./features/AuthStack/Introduce').default} />
      <AuthStack.Screen name={'Login'} component={require('./features/AuthStack/Login').default} />
      <AuthStack.Screen name={'Register'} component={require('./features/AuthStack/Register').default} />
      <AuthStack.Screen name={'ResetPassword'} component={require('./features/AuthStack/ResetPassword').default} />
      <AuthStack.Screen name={'CheckOTP'} component={require('./features/AuthStack/ResetPassword/CheckOTP').default} />
      <AuthStack.Screen name={'ForgotPassword'} component={require('./features/AuthStack/ResetPassword/ForgotPassword').default} />
   </AuthStack.Navigator>
  );
});

const MainStackComponent = memo(function MainStackComponent() {
  return (
    <MainStack.Navigator initialRouteName={'MainStack'} headerMode={'none'}>
      <MainStack.Screen name={'MainStack'} component={require('./features/MainStack').default} />
      <MainStack.Screen name={'UpdateInfo'} component={require('./features/MainStack/Account/UpdateInfo').default} />
      <MainStack.Screen name={'OtherAccount'} component={require('./features/MainStack/Account/OtherAccount').default} />
      <MainStack.Screen name={'ChangePassword'} component={require('./features/MainStack/Account/ChangePassword').default} />

      <MainStack.Screen name={'Question'} component={require('./features/MainStack/Question').default} />
      <MainStack.Screen name={'CreateQuestion'} component={require('./features/MainStack/Question/CreateQuestion').default} />
      <MainStack.Screen name={'AddQuestion'} component={require('./features/MainStack/Question/AddQuestion').default} />
      <MainStack.Screen name={'SolvedQuestion'} component={require('./features/MainStack/Question/SolvedQuestion').default} />
      <MainStack.Screen name={'ShuffleQuestion'} component={require('./features/MainStack/Question/ShuffleQuestion').default} />
      <MainStack.Screen name={'QuestionDetail'} component={require('./features/MainStack/Question/Detail').default} />
      <MainStack.Screen name={'AllQuestion'} component={require('./features/MainStack/Question/AllQuestion').default} />
      <MainStack.Screen name={'SearchQuestion'} component={require('./features/MainStack/Question/SearchQuestion').default} />
      <MainStack.Screen name={'MyQuestion'} component={require('./features/MainStack/Question/MyQuestion').default} />
      
      <MainStack.Screen name={'Package'} component={require('./features/MainStack/Package').default} />
      <MainStack.Screen name={'DoPackage'} component={require('./features/MainStack/Package/DoPackage').default} />
      <MainStack.Screen name={'AddPackage'} component={require('./features/MainStack/Package/AddPackage').default} />
      <MainStack.Screen name={'SolvedPackage'} component={require('./features/MainStack/Package/SolvedPackage').default} />
      <MainStack.Screen name={'PackageDetail'} component={require('./features/MainStack/Package/Detail').default} />
      <MainStack.Screen name={'PackageHistory'} component={require('./features/MainStack/Package/History').default} />
      <MainStack.Screen name={'CreatePackage'} component={require('./features/MainStack/Package/CreatePackage').default} />
      <MainStack.Screen name={'SubmitPackage'} component={require('./features/MainStack/Package/SubmitPackage').default} />
      <MainStack.Screen name={'PackageHistoryDetail'} component={require('./features/MainStack/Package/PackageHistoryDetail').default} />
      
      <MainStack.Screen name={'Definition'} component={require('./features/MainStack/Dictionary/Definition').default} />

      <MainStack.Screen name={'SideMenu'} component={require('./features/MainStack/Drawer/SideMenu').default} />
      <MainStack.Screen name={'PlaySound'} component={require('./features/MainStack/PlaySound').default} />
    </MainStack.Navigator>
  );
});

export const Routes = memo(function Routes() {
  const routeNameRef = useRef('');
  const onStateChange = useCallback(() => {
    const previousRouteName = routeNameRef.current;
    const currentRouteName = navigationRef.current?.getCurrentRoute()?.name;

    if (currentRouteName && previousRouteName !== currentRouteName) {
      routeNameRef.current = currentRouteName;
    }
  }, []);

  return (
    <>
      <NavigationContainer
        ref={navigationRef}
        onStateChange={onStateChange}>
        <RootStack.Navigator initialRouteName="AuthStack" headerMode={'none'}>
          <RootStack.Screen name={'AuthStack'} component={AuthStackComponent} />
          <RootStack.Screen name={'MainStack'} component={MainStackComponent} />
        </RootStack.Navigator>
      </NavigationContainer>
    </>
  );
});
